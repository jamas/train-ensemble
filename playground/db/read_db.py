import json
import re

import ase.db
from tqdm import tqdm

from train_ensemble.calculate_atomization import calculate_atomization_energy

out_db_path = "test.db"
dbs = open("dbs").readlines()

count = 0

open('index.txt','w')

with ase.db.connect(out_db_path) as out_db:
    for input_db_path in tqdm(dbs):
        rxn = re.search(
            "rxn\d+", input_db_path  # pylint: disable=anomalous-backslash-in-string
        ).group()
        with ase.db.connect(input_db_path.strip()) as db:
            start = count
            symbols = None
            for i, row in enumerate(db.select()):
                if not symbols:
                    symbols = str(row.toatoms().symbols)

                count += 1
                data = row.data
                ae = calculate_atomization_energy(row)
                U0 = row.energy
                row.energy = ae
                data["U0"] = ae
                data["rxn"] = rxn
                out_db.write(row.toatoms(), data=data)
            end = count

            with open('index.txt','a') as index:
                index.write(f'{rxn}: {start}:{end} - {symbols}\n')












print(count)
