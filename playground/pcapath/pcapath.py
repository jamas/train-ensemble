import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
import torch
from ase.io import iread
from sklearn.decomposition import PCA

from train_ensemble.data import atoms_to_batch

path = "/Users/jamas/phd/research/train-ensemble/pipelines/neuroneb/neb/10may/painn1/test/rxn0292/output/mep.xyz"
path = "/Users/jamas/phd/research/train-ensemble/pipelines/neuroneb/neb/giant_neb/painn1/test/rxn0047/output/mep.xyz"

ats = list(iread(
    path
))
mep = [at.get_potential_energy() for at in ats]
min_ = min(mep)
max_ = max(mep) + 2

example_at = ats[0]


batch =atoms_to_batch(ats)

pos = np.stack([at.positions for at in ats])
shape = pos.shape
pos = pos.reshape(shape[0], np.prod(shape[1:]))

pca = PCA(n_components=2)
pca.fit(pos)
pca_pos = pca.transform(pos)

model = torch.load(
    "/Users/jamas/phd/research/train-ensemble/pipelines/neuroneb/models/train.v1/models/painn1/train/output/model/best"
)

x_ = pca_pos[:, 0]
y_ = pca_pos[:, 1]

data_original = np.dot(pca_pos[0], pca.components_) + pca.mean_

xs = np.linspace(min(x_) - 0.30, max(x_) + 0.30, 200)
ys = np.linspace(min(y_) - 0.30, max(y_) + 0.30, 200)

fun_map = np.empty((xs.size, ys.size))

for i, x in tqdm(enumerate(xs)):
    batch = np.stack([x * np.ones_like(ys), ys], axis=1)
    batch_original = np.dot(batch, pca.components_) + pca.mean_
    batch_original = batch_original.reshape(batch.shape[0], *shape[1:])

    ats_batch = []
    for pos in batch_original:
        at = example_at.copy()
        at.set_positions(pos)
        ats_batch.append(at)

    ready_batch = atoms_to_batch(ats_batch)
    es = model(ready_batch)['energy'].detach().numpy().squeeze()
    fun_map[:, i] = es

fun_map = fun_map.clip(min_, max_)


fig, ax = plt.subplots(figsize=(15, 15))
ax.imshow(fun_map, extent=(xs[0], xs[-1], ys[0], ys[-1]), origin="lower")
ax.scatter(x_, y_, c='k', marker='x')
ax.plot(x_, y_, linestyle='--', c='k')


png_path = path.replace('mep.xyz', 'pca.png')
# fig.colorbar(im)
fig.savefig("png_path")



# plt.scatter(x, y, c="k")
plt.show()
