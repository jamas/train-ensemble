import chem_utils

import ase.db

print(len(ase.db.connect('qm9x.db')))
row = next(ase.db.connect('qm9x.db').select('id=4'))
print(row.formula)
print(row.data['U0'])
atoms = row.toatoms()

atoms.calc = chem_utils.calc()

atoms.get_forces()
print(atoms.get_potential_energy())
