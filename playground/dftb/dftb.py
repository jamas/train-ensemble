import os

from ase import Atoms
from ase.calculators.dftb import Dftb
from ase.io import iread

from train_ensemble import calculator


sk = "/Users/jamas/phd/research/train-ensemble/storage/slater-koster/"

os.environ["ASE_DFTB_COMMAND"] = "/usr/local/Caskroom/miniforge/base/bin/dftb+ > PREFIX.out"
ASE_DFTB_COMMAND = "/usr/local/Caskroom/miniforge/base/bin/dftb+ PREFIX.in > PREFIX.out"
# ASE_DFTB_COMMAND="/usr/local/Caskroom/miniforge/base/bin/dftb+ > PREFIX.out"
# export ASE_DFTB_COMMAND="/path/to/dftb+ > PREFIX.out"

at = next(iread('/Users/jamas/phd/research/train-ensemble/storage/rxns_mini/rxn000000/product.xyz'))


# at.calc = Dftb(slako_dir="/Users/jamas/phd/research/train-ensemble/storage/slater-koster/")
at.calc = calculator.get_dftb_calculator()
print(at.get_potential_energy())
pass
