import torch
from ase.io import Trajectory

from train_ensemble.calculator import MLCalculator
from train_ensemble.calculator import get_ml_calc_constructor


traj_path = "/Users/jamas/phd/research/train-ensemble/pipelines/neb_big_painn/jobs/traj_rxn000000/output/neb.traj"
traj = Trajectory(traj_path)

model = torch.load('/Users/jamas/phd/research/train-ensemble/pipelines/painn_ensemble/jobs/models/painn1/instantiate/output/model.pt')
for i, atoms in enumerate(traj):
    print(i)
    if i >20:
        __import__("pdb").set_trace() #TODO delme

atoms = next(traj.__iter__())

atoms.calc = get_ml_calc_constructor(model, 'gnn')()

atoms.get_potential_energy()
atoms.calc.results

__import__("pdb").set_trace() #TODO delme
traj = Trajectory('blah.traj', 'a', atoms)
traj.write()

tr = Trajectory('blah.traj', 'r')

a = next(tr.__iter__())
