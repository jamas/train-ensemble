import ase.db
import matplotlib.pyplot as plt
import numpy as np
from dscribe.descriptors import CoulombMatrix
from sklearn.cluster import KMeans
import itertools
from itertools import chain, repeat, count
from sklearn.decomposition import PCA


def main():
    neb_db = "/Users/jamas/phd/research/train-ensemble/pipelines/dft/100_500bfgs/jobs/rxn000101/output/neb.db"
    # neb_db = "/Users/jamas/phd/research/train-ensemble/neb.db"

    print("creating cms")
    data = get_cms(neb_db)

    pca = PCA(n_components=2)
    pca.fit(data)

    n = len(data)
    labels = np.array(list(chain.from_iterable(repeat(range(10),  n // 10))))

    clusters = get_clusters(data, labels)

    for cluster in clusters:
        pca_cluster = pca.transform(cluster)
        plt.scatter(*zip(*pca_cluster))

    _, axes = plt.subplots(4,2)

    clusters = get_clusters(data, labels)
    for i, (cluster, ax) in enumerate(zip(clusters[1:9],  axes.flat)):
        pca.fit(cluster)
        pca_cluster = pca.transform(cluster)
        ax.scatter(*zip(*pca_cluster))

        sub_cluster_labels = get_labels(cluster, len(cluster) // 2)
        sub_clusters = get_clusters(cluster, sub_cluster_labels)

        for sub_cluster in sub_clusters:
            pca_cluster = pca.transform(sub_cluster)
            ax.scatter(*zip(*pca_cluster))

    plt.savefig('fig.png')
    plt.show()


def weed_out_data(data):
    n = len(data)
    if n == 1:
        return data

    labels = get_labels(data, n // 2)
    clusters = get_clusters(data, labels)
    if not clusters:
        return np.array([])
    new_clusters = _weed_out_data(clusters)
    new_cluster = np.concatenate(new_clusters, axis=0)

    return new_cluster


def _weed_out_data(clusters):
    cluster_densities = get_cluster_densities(clusters)
    if not cluster_densities:
        return clusters

    mean_rho = np.mean(cluster_densities)
    removed = 0

    new_clusters = []
    for cluster, rho in zip(clusters, cluster_densities):
        new_cluster = cluster
        if rho == 0:
            pass

        elif mean_rho / rho < 1:
            ratio = mean_rho / rho
            remove = int((1 - ratio) * len(cluster))
            np.random.shuffle(cluster)
            new_cluster = cluster[remove:]
            removed += remove

        new_clusters.append(new_cluster)

    return new_clusters


    # plt.show()

    # n_clusters = 10

    # ds = []

    # for i in range(1):
    #     clusters = get_clusters(clusters, n_clusters)
    #     clusters = weed_out_data(clusters)
    #     densities = get_cluster_densities(clusters)
    #     densities.sort()
    #     ds.append(densities)

    # pca_plot(clusters)

    # fig, ax = plt.subplots()
    # for d in ds:
    #     plt.plot(d)
    # plt.show()

    # print("done")


def get_pca(data):
    pca = PCA(n_components=2)
    pca.fit(data)
    x = pca.transform(data)
    return x


#
# def pca_plot(data):
#     if isinstance(data, list):
#         data = np.concatenate(data, axis=0)
#
#     pca = PCA(n_components=2)
#     pca.fit(data)
#     x = pca.transform(data)
#     return zip(*x)
#
#     fig, ax = plt.subplots()
#     y, z = zip(*x)
#     ax.scatter(y, z)


def get_labels(data, n_clusters):
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(data)
    return kmeans.labels_


def get_clusters(data, labels):
    clusters = []
    for i in range(max(labels)):
        cluster = data[labels == i]
        clusters.append(cluster)
    return clusters


# def get_clusters(data, n_clusters):
#     if isinstance(data, list):
#         data = np.concatenate(data, axis=0)
#     print(len(data))
#
#     kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(data)
#     clusters = []
#     for i in range(n_clusters):
#         cluster = data[kmeans.labels_ == i]
#         clusters.append(cluster)
#
#     return clusters, kmeans.labels_


def get_positions(path):
    positions = []
    with ase.db.connect(path) as db:
        data = db.select("")
        for row in data:
            atoms = row.toatoms()
            positions.append(atoms.positions.flatten())

    return np.array(positions)


def get_cms(path):
    cms = []
    with ase.db.connect(path) as db:
        data = db.select("")
        first = True
        for row in data:
            atoms = row.toatoms()
            if first:
                first = False
                n_atoms = len(atoms.get_chemical_symbols())
                cm_calc = CoulombMatrix(n_atoms, permutation="none")

            cm = cm_calc.create(atoms)
            cms.append(cm)

    return np.array(cms)


def get_cluster_density(cluster):
    if len(cluster) == 1:
        return 0
    centroid = cluster.mean(axis=0)
    cluster = cluster - centroid
    mean_norm = np.linalg.norm(cluster, axis=1).mean()
    if mean_norm == 0:
        return 0.9999999
    rho = len(cluster) / mean_norm
    return rho


def get_cluster_densities(clusters):
    cluster_densities = []
    for cluster in clusters:
        cluster_densities.append(get_cluster_density(cluster))

    return cluster_densities


# def plot_clusters(clusters):
#     cluster_densities = []
#     for cluster in clusters:
#         cluster_densities.append(cluster_density(cluster))
#
#     max_rho = max(cluster_densities)
#     mean_rho = np.mean(cluster_densities)
#     fig, ax = plt.subplots()
#     for cluster, rho in zip(clusters, cluster_densities):
#         ax.scatter(cluster[:, 0], cluster[:, 1], c=str(rho / max_rho))




# clusters = []
# for i in range(n_clusters):
#     cluster = dp[kmeans.labels_ == i]
#     clusters.append(cluster)
#
#
# d = []
#
# for i in range(4):
#     densities = get_cluster_densities(clusters)
#     densities.sort()
#     d.append(densities)
#
#     plot_clusters(clusters)
#     clusters = weed_out_data(clusters)
#
#
# fig, ax = plt.subplots()
# for i in d:
#     ax.plot(range(len(i)), i)
#
#
# mep = "/Users/jamas/pipelines/rxns/1500_4000/jobs/rxn002182/output/mep.xyz"
#
#
# atoms = list(iread(mep))[1]
# n_atoms = len(atoms.get_atomic_numbers())
main()
