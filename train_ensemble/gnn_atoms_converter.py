from train_ensemble import gdata
from train_ensemble.utils import DEVICE

class DummyRow:
    def __init__(self, atoms):
        self.atoms = atoms.copy()

    def toatoms(self):
        return self.atoms


class AtomsGNNConverter:
    def __init__(self):
        self.row_to_xyz = gdata.TransformRowToGraphXyz()

    def __call__(self, atoms):
        dummyrow = DummyRow(atoms)
        atoms = self.row_to_xyz(dummyrow)
        return gdata.collate_atomsdata([atoms], pin_memory=(DEVICE == "cuda"))
