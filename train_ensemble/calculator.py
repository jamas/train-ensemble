import os

import numpy as np
from train_ensemble import data, utils

from ase.calculators.calculator import Calculator, all_changes
from ase.calculators.dftb import Dftb
from ase.calculators.orca import ORCA
from schnetpack.data.atoms import AtomsConverter


def get_dft_calc(orcabinary=None, label=None, workdir=None):
    if not orcabinary:
        orcabinary = "orca"
    if not label:
        label = "orca"
    if not workdir:
        workdir = "orca"

    os.environ["ASE_ORCA_COMMAND"] = f"{orcabinary} PREFIX.inp > PREFIX.out"
    os.makedirs(workdir, exist_ok=True)

    return ORCA(
        label=os.path.join(workdir, label),
        orcasimpleinput="wB97X 6-31G(d)",
    )


class OrcaCalcFactory:
    def __init__(self, orcadir=None, orcabinary=None):
        self.orcadir = orcadir if orcadir else "orca/"
        self.orcabinary = orcabinary if orcabinary else "orca"

        os.makedirs(self.orcadir, exist_ok=True)
        os.environ["ASE_ORCA_COMMAND"] = f"{self.orcabinary} PREFIX.inp > PREFIX.out"

    def __call__(self, cores, label):
        # could take an orcasimpleinput
        orcablocks = f"%pal nprocs {cores} end" if cores > 1 else ""

        return ORCA(
            label=os.path.join(self.orcadir, label),
            orcasimpleinput="wB97X 6-31G(d)",
            orcablocks=orcablocks,
        )


def get_ml_calc_constructor(
    model, atoms_converter, implemented_properties=None, device=None, batch_handler=None
):
    if atoms_converter == "gnn":
        from train_ensemble.gnn_atoms_converter import (
            AtomsGNNConverter,
        )  # pylint: disable=import-outside-toplevel

        atoms_converter = AtomsGNNConverter()

    elif atoms_converter == "spk":
        atoms_converter = AtomsConverter()

    else:
        raise ValueError("atoms_converter has to be gnn or spk")

    return lambda: MLCalculator(
        model,
        atoms_converter=atoms_converter,
        implemented_properties=implemented_properties,
        batch_handler=batch_handler,
        device=device,
    )


def get_dftb_calc_construtor(
    slako_dir=None, dftb_binary=None, label=None, workdir=None
):
    return lambda: get_dftb_calculator(slako_dir=slako_dir, dftb_binary=dftb_binary, label=label, workdir=workdir)


def get_dftb_calculator(slako_dir=None, dftb_binary=None, label=None, workdir=None):
    if not slako_dir:
        slako_dir = "/Users/jamas/phd/research/train-ensemble/storage/slater-koster/"
    assert slako_dir.endswith("/"), 'slako_dir must be a directory ending with "/"'

    if not workdir:
        workdir = "dftb"

    if not dftb_binary:
        dftb_binary = "/usr/local/Caskroom/miniforge/base/bin/dftb+"

    if not label:
        label = "dftb"

    os.environ["ASE_DFTB_COMMAND"] = f"{dftb_binary} > PREFIX.out"

    return Dftb(
        Hamiltonian_="DFTB",  # line is included by default
        Hamiltonian_SCC="Yes",
        Hamiltonian_SCCTolerance=1e-8,
        slako_dir=slako_dir,
        label=os.path.join(workdir, label)
    )


class BatchHandler:
    def __init__(self, atoms):
        self.atoms = atoms

    def get_batch(self):
        return data.atoms_to_batch(self.atoms)


class MLCalculator(Calculator):
    def __init__(
        self,
        model,
        # atoms_converter,
        implemented_properties=None,
        device=None,
        batch_handler=None,
        **kwargs,
    ):
        if not implemented_properties:
            implemented_properties = ["energy", "energy_var", "forces", "forces_var"]
        self.implemented_properties = implemented_properties

        self.batch_handler = batch_handler

        super().__init__(**kwargs)

        # self.atoms_converter = atoms_converter
        self.model = model
        self.device = device
        if device:
            model.to(device)

    def calculate(
        self, atoms=None, properties=None, system_changes=None
    ):  # pylint:disable=unused-argument

        if not system_changes:
            system_changes = all_changes

        if not properties:
            properties = ["energy", "forces"]

        if self.calculation_required(atoms, properties):
            super().calculate(atoms)

            # atoms = self.atoms_converter(atoms)
            batch_handler = (
                self.batch_handler if self.batch_handler else BatchHandler([atoms])
            )

            batch = batch_handler.get_batch()

            if self.device:
                batch = utils.batch_to_device(batch, self.device)

            results = self.model(batch)
            # energy = results["energy"].item()
            energies = np.array(results["energy"].cpu().detach().numpy().squeeze(1))
            forces = np.array(results["forces"].cpu().detach().numpy())

            for force, energy, atom in zip(forces, energies, batch_handler.atoms):
                atom.calc.results = {
                    "energy": energy,
                    "forces": force,
                }  # pylint:disable=attribute-defined-outside-init

                if "energy_var" in results:
                    atoms.calc.results["energy_var"] = results["energy_var"].item()
                if "forces_var" in results:
                    atoms.calc.results["forces_var"] = np.array(
                        results["forces_var"].cpu().squeeze().detach().numpy()
                    )

            for atom in batch_handler.atoms:
                atom.calc.atoms = atom.copy()
