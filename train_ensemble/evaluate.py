import numpy as np
import torch

from train_ensemble import utils


def eval_model(model, dataloader, loss_fn=None, logging_steps=float("inf")):

    if not loss_fn:
        def loss_fn(*_, **__):
            return torch.Tensor([0])

    running_loss = running_count = 0

    target_energy = np.array([])
    model_energy = np.array([])
    target_forces = np.array([])
    model_forces = np.array([])

    for i, batch in enumerate(dataloader):
        batch = utils.batch_to_device(batch, utils.DEVICE)
        result = model(batch)

        target_energy = np.append(
            target_energy, batch["energy"].cpu().flatten().detach().numpy()
        )
        model_energy = np.append(
            model_energy, result["energy"].cpu().flatten().detach().numpy()
        )

        target_forces = np.append(
            target_forces, batch["forces"].cpu().flatten().detach().numpy()
        )
        model_forces = np.append(
            model_forces, result["forces"].cpu().flatten().detach().numpy()
        )

        with torch.no_grad():
            loss = loss_fn(
                batch=batch,
                result=result,
            )
            running_loss += loss
        running_count += dataloader.batch_size

        if i >= logging_steps:
            break

    energy_errors = target_energy - model_energy
    energy_rmse = np.mean(energy_errors ** 2) ** 0.5
    energy_mae = np.mean(np.abs(energy_errors))

    forces_errors = target_forces - model_forces
    forces_rmse = np.mean(forces_errors ** 2) ** 0.5
    forces_mae = np.mean(np.abs(forces_errors))

    loss = running_loss / running_count

    return loss.item(), {
        "val_loss": loss.item(),
        "energy_rmse": energy_rmse,
        "energy_mae": energy_mae,
        "forces_rmse": forces_rmse,
        "forces_mae": forces_mae,
    }
