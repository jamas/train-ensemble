import torch

from train_ensemble.utils import DEVICE
from train_ensemble.model import get_schnet


class Ensemble(torch.nn.Module):
    def __init__(self, models, energy_key, force_key, scaling=1):
        super().__init__()

        self.models = torch.nn.ModuleList(
            [torch.load(model) for model in models]
        )

        self.energy_key = energy_key
        self.force_key = force_key
        self.scaling = scaling

    def forward(self, x):
        ensemble_prediction = {}

        model_predictions = [model(x) for model in self.models]

        for key in [self.energy_key, self.force_key]:
            ensemble_prediction[key] = torch.stack(
                [model_prediction[key] for model_prediction in model_predictions]
            ).mean(axis=0)
            ensemble_prediction[key + "_var"] = torch.stack(
                [model_prediction[key] for model_prediction in model_predictions]
            ).var(axis=0) * self.scaling

        return ensemble_prediction

    def set_scaling(self, scaling):
        self.scaling = scaling
