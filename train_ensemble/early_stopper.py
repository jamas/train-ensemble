import json
import os

import torch

from train_ensemble import utils


class EarlyStopper:
    """ Instantiates with a model and an output dir. Step function takes a loss, and everytime "\
            f"we find a new best loss we save the model. Also the latest model is saved """

    def __init__(self, model, output_dir):
        self.model = model
        self.output_dir = output_dir
        self.model_dir = os.path.join(output_dir, 'model')
        self.best = float("inf")

        os.makedirs(self.model_dir, exist_ok=True)

    def step(self, loss, log):
        if loss < self.best:
            self.best = loss
            self.save_model("best")
            with open(os.path.join(self.output_dir, "best.json"), "w") as f:
                json.dump(log, f)

        self.save_model("latest")
        with open(os.path.join(self.output_dir, "latest.json"), "w") as f:
            json.dump(log, f)

    def save_model(self, name):
        self.model.to("cpu")
        torch.save(
            self.model.state_dict(), os.path.join(self.model_dir, name + ".pth")
        )
        torch.save(self.model, os.path.join(self.model_dir, name))
        self.model.to(utils.DEVICE)
