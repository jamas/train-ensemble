import json
import os

import ase
import numpy as np
import torch
from ase.io import iread
from rmsd import kabsch_rmsd

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"


def translate(lst):
    shift = lst[0]
    return np.array(lst) - shift


def load_json(path):
    return json.load(open(path, "r"))


def read_xyz(path):
    if os.path.isdir(path):
        files = os.listdir(path)

        xyz_filter = lambda x: x.endswith("xyz")
        xyz_files = list(filter(xyz_filter, files))
        xyz_files.sort()

        return [read_xyz(os.path.join(path, fname)) for fname in xyz_files]

    atoms = list(iread(path))

    if len(atoms) == 1:
        return atoms[0]
    return atoms


def dump_json(object_, path):
    json.dump(object_, open(path, "w"))


def cast_dict(data, type_):
    data_ = {}
    if isinstance(data, dict):
        for key, val in data.items():
            data_[key] = cast_dict(val, type_)
    else:
        return type_(data)
    return data_


def batch_to_device(batch, device):
    return {k: v.to(device) for k, v in batch.items()}


def distance(at1, at2):
    if isinstance(at1, ase.Atoms):
        at1 = at1.positions

    if isinstance(at2, ase.Atoms):
        at2 = at2.positions

    return kabsch_rmsd(at1, at2, translate=True)
