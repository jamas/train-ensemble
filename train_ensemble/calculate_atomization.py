from ase.units import Hartree


ENERGIES = {
    "H": -0.500607632585*Hartree,
    "C": -37.8302333826*Hartree,
    "N": -54.5680045287*Hartree,
    "OX": -75.0362229210*Hartree,
}

AT_NUM = {
    1: "H",
    6: "C",
    7: "N",
    8: "OX",
}

ATREFS = [0] * 9
for idx, code in AT_NUM.items():
    ATREFS[idx] = ENERGIES[code]


def calculate_atomization_energy(row):
    atomization_energy = row.data["energy"]
    atomnumbers = row.numbers

    for atomnumber in atomnumbers:
        atom_energy = ATREFS[atomnumber]
        atomization_energy = atomization_energy - atom_energy

    return atomization_energy
