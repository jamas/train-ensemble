import pandas as pd

class PandasLogger:
    def __init__(self, logging_path, previous_logs=None):
        self.logging_path = logging_path
        if previous_logs:
            self.dataframe = pd.read_csv(previous_logs, index_col=0)
        else:
            self.dataframe = pd.DataFrame()

    def step(self, series):
        self.dataframe = self.dataframe.append(series, ignore_index=True)
        self.dataframe.to_csv(self.logging_path)
