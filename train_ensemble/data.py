import json
import os
from functools import partial

import numpy as np
import schnetpack as spk
import torch

from train_ensemble import gdata
from train_ensemble.gdata import TransformRowToGraphXyz, collate_atomsdata

class DummyRow():
    def __init__(self, atoms):
        self.atoms = atoms

    def toatoms(self):
        return self.atoms

transform = TransformRowToGraphXyz()
def atoms_to_batch(atoms, pin_memory=False):

    dummyrows = [DummyRow(atom) for atom in atoms]
    graphdata = [transform(row) for row in dummyrows]
    return collate_atomsdata(graphdata, pin_memory)




def create_split(dataset, num_train, num_val, split_path):
    """
    dataset: The dataset to split
    split: Float from 0 to 1, how much data should be dedicated to train vs validation
    split_path: Path to save split file
    n_datapoints: how many datapoints should we use for training and validation, rest is saved in
    test dataset, if None, all datapoints are used
    """

    if os.path.exists(split_path):
        os.remove(split_path)

    train_dataset, val_dataset, test_dataset = spk.train_test_split(
        data=dataset, num_train=num_train, num_val=num_val, split_file=split_path
    )

    return train_dataset, val_dataset, test_dataset


def read_split(dataset, split_path):
    """
    dataset: Dataset,
    splitfile: path to split file
    """

    train_dataset, val_dataset, test_dataset = spk.train_test_split(
        data=dataset, split_file=split_path
    )

    return train_dataset, val_dataset, test_dataset


def get_spk_dataloaders(
    data, split, batch_size=100, energy_key="energy", forces_key="forces"
):
    dataset = get_dataset(
        data,
        [energy_key, forces_key],
    )

    train_dataset, val_dataset, test_dataset = read_split(
        dataset,
        split,
    )

    train_loader = spk.AtomsLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_loader = spk.AtomsLoader(val_dataset, batch_size=batch_size, shuffle=True)
    test_loader = spk.AtomsLoader(test_dataset, batch_size=batch_size)

    return train_loader, val_loader, test_loader


def get_gnn_dataloaders(
    data, split, batch_size=100, energy_key="energy", forces_key="forces"
):
    dataset = gdata.AseDbData(
        data,
        gdata.TransformRowToGraphXyz(
            cutoff=5.0,
            energy_property=energy_key,
            forces_property=forces_key,
        ),
    )

    split = np.load(split)

    collate = partial(gdata.collate_atomsdata, pin_memory=torch.cuda.is_available())
    loaders = []

    for data_split in ["train", "val", "test"]:
        idx = split[f"{data_split}_idx"].tolist()
        if not idx:
            loaders.append(None)
            continue
        subset = torch.utils.data.Subset(dataset, idx)
        loader = torch.utils.data.DataLoader(
            subset,
            batch_size,
            sampler=torch.utils.data.RandomSampler(subset),
            collate_fn=collate,
        )
        loaders.append(loader)

    return loaders


def get_datasets(data_configs):
    datasets = []
    if isinstance(data_configs, str):
        data_configs = {"db": data_configs}

    if isinstance(data_configs, dict):
        data_configs = [data_configs]

    for data_config in data_configs:
        dataset = get_dataset(**data_config)
        datasets += [dataset]

    return datasets


def get_dataset(db, idx=None, energy_key="energy", forces_key="forces"):
    dataset = gdata.AseDbData(
        db,
        gdata.TransformRowToGraphXyz(
            cutoff=5.0,
            energy_property=energy_key,
            forces_property=forces_key,
        ),
    )

    if idx:
        if isinstance(idx, str):
            idx = json.load(open(idx))
        dataset = torch.utils.data.Subset(dataset, idx)

    return dataset


def get_datasplits(dataset, split):
    split = np.load(split)
    subsets = []
    for data_split in ["train_idx", "val_idx", "test_idx"]:
        if data_split in split:
            idx = split[data_split].tolist()
            subset = torch.utils.data.Subset(dataset, idx)
            subsets.append(subset)
        else:
            subsets.append(None)

    return subsets


def get_dataloader(dataset, batch_size, shuffle=True):
    if not dataset:
        return None

    collate = partial(gdata.collate_atomsdata, pin_memory=torch.cuda.is_available())
    loader = torch.utils.data.DataLoader(
        dataset,
        batch_size,
        collate_fn=collate,
        shuffle=shuffle,
    )

    return loader
