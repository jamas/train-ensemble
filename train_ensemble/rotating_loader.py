from itertools import repeat, chain


def cycle(iterable):
    return chain.from_iterable(repeat(iterable))


class RotatingDataLoader:
    def __init__(self, dataloaders):
        longest_idx = dataloaders.index(max(dataloaders, key=len))
        longest = dataloaders.pop(longest_idx)
        self.longest = longest
        self.dataloaders = [longest] + [cycle(dataloader) for dataloader in dataloaders]
        self.batch_size = longest.batch_size

    def __len__(self):
        return len(self.longest) * len(self.dataloaders)

    def __iter__(self):
        for batches in zip(*self.dataloaders):
            for batch in batches:
                yield batch
