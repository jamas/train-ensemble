import json
import os
import pickle as pkl
import uuid
from concurrent.futures import ThreadPoolExecutor

import matplotlib.pyplot as plt
import numpy as np
import torch
from scipy import interpolate
from train_ensemble import calculator
from train_ensemble.data import atoms_to_batch

from ase import Atoms
from ase.io import iread
from pype import BaseConfig


class MLConfig(BaseConfig):
    script_path = "jobs/plot_mep.py"
    inputs = {"model", "mep.xyz"}
    optional_inputs = {"mep2.xyz"}
    params = {"show": False, "title": None}
    outputs = {
        "fig.pkl": "fig.pkl",
        "grid.json": "grid.json",
        "mep.json": "mep.json",
        "mep.png": "mep.png",
        'plot.json': 'plot.json',
    }


class DFTBConfig(BaseConfig):
    script_path = "jobs/plot_mep.py"
    inputs = {"mep.xyz"}
    optional_inputs = {"dftb_binary", "slako_dir", "mep2.xyz"}
    params = {"show": False, "title": None, "workdir": None}
    outputs = {
        "fig.pkl": "fig.pkl",
        "grid.json": "grid.json",
        "mep.json": "mep.json",
        "mep.png": "mep.png",
        'plot.json': 'plot.json',
    }


class DFTConfig(BaseConfig):
    script_path = "jobs/plot_mep.py"
    inputs = {"mep.xyz"}
    optional_inputs = {"orcabinary", "mep2.xyz"}
    params = {"show": False, "title": None, "workdir": None, "max_workers": 1}
    outputs = {
        "fig.pkl": "fig.pkl",
        "grid.json": "grid.json",
        "mep.json": "mep.json",
        "mep.png": "mep.png",
        'plot.json': 'plot.json',
    }


def calculate_dftb_energies(config, atoms):
    for ats in atoms:
        ats.calc = calculator.get_dftb_calculator(
            slako_dir=config["inputs"]["slako_dir"],
            dftb_binary=config["inputs"]["dftb_binary"],
            label=f'dftb/{uuid.uuid4().hex}',
            workdir=config['params']['workdir'],
        )

    return np.array([ats.get_potential_energy() for ats in atoms])


def dft_task(at):
    at.get_potential_energy()


def calculate_dft_energies(config, atoms):
    for i, ats in enumerate(atoms):
        ats.calc = calculator.get_dft_calc(
            config["inputs"]["orcabinary"],
            f"orca{i}/orca",
            config["params"]["workdir"],
        )

    with ThreadPoolExecutor(max_workers=config["params"]["max_workers"]) as executor:
        executor.map(dft_task, atoms)

    # if config["params"]["workdir"]:
    # shutil.rmtree(config["params"]["workdir"])

    return np.array([ats.get_potential_energy() for ats in atoms])


def main(config):
    ats = list(iread(config["inputs"]["mep.xyz"]))

    atom_factory = AtomFactory(ats[0])
    mep = [at.positions.flatten() for at in ats]

    p = ats[-1].positions
    r = ats[0].positions
    ts = max(ats, key=lambda x: x.get_potential_energy()).positions

    p = p.flatten()
    r = r.flatten()
    ts = ts.flatten()

    plane = Plane(p, r, ts)
    projected_mep = plane.project2d(mep)

    nd_mep = plane.transform_2d_to_nd(projected_mep)

    grid_size = 40
    xlim, ylim = find_lim(*projected_mep, buf=0.2)

    grid = grid_from_limits(xlim, ylim, grid_size)
    nd_grid = plane.transform_2d_to_nd(grid)

    grid_atoms = [atom_factory(pt) for pt in nd_grid]
    mep_atoms = [atom_factory(pt) for pt in nd_mep]

    if os.path.exists(config["outputs"]["grid.json"]):
        grid_es = np.array(json.load(open(config["outputs"]["grid.json"])))
        mep_es = np.array(json.load(open(config["outputs"]["mep.json"])))

    elif config["qualname"] == DFTBConfig.__qualname__:
        grid_es = calculate_dftb_energies(config, grid_atoms)
        mep_es = calculate_dftb_energies(config, mep_atoms)

    elif config["qualname"] == DFTConfig.__qualname__:
        grid_es = calculate_dft_energies(config, grid_atoms)
        mep_es = calculate_dft_energies(config, mep_atoms)

    elif config["qualname"] == MLConfig.__qualname__:
        model = torch.load(config["inputs"]["model"])
        grid_batch = atoms_to_batch(grid_atoms)
        mep_batch = atoms_to_batch(mep_atoms)

        grid_es = model(grid_batch)["energy"].detach().numpy().squeeze()
        mep_es = model(mep_batch)["energy"].detach().numpy().squeeze()

    json.dump(grid_es.tolist(), open(config["outputs"]["grid.json"], "w"))
    json.dump(mep_es.tolist(), open(config["outputs"]["mep.json"], "w"))

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")

    min_e = min(mep_es)
    max_e = max(mep_es)
    diff_e = (max_e - min_e) * 0.75
    grid_es = grid_es.clip(min_e - diff_e, max_e + diff_e)

    x, y = zip(*grid)
    z = grid_es

    interpolated_path = interpolate_path(*zip(*projected_mep), mep_es.tolist())

    surf = ax.plot_trisurf(x, y, z, cmap="viridis", alpha=0.9)
    fig.colorbar(surf, shrink=0.5, aspect=10)
    ax.scatter(*zip(*projected_mep), mep_es, s=50, marker="o", c="k", depthshade=False)
    ax.plot(*zip(*interpolated_path), c="k", linestyle="--")

    ax.set_xlabel("$u_1 / Å$")
    ax.set_ylabel("$u_2 / Å$")
    ax.set_zlabel("E / eV")

    if config["params"]["title"]:
        ax.set_title(config["params"]["title"])

    pkl.dump(fig, open(config["outputs"]["fig.pkl"], "wb"))
    ax.view_init(70, 60)

    # If second mep
    if config["inputs"]["mep2.xyz"]:
        ats2 = list(iread(config["inputs"]["mep2.xyz"]))
        mep2 = [at.positions.flatten() for at in ats2]
        projected_mep2 = plane.project2d(mep2)
        nd_mep2 = plane.transform_2d_to_nd(projected_mep2)
        mep2_atoms = [atom_factory(pt) for pt in nd_mep2]

        if config["qualname"] == DFTBConfig.__qualname__:
            mep2_es = calculate_dftb_energies(config, mep2_atoms)
        elif config["qualname"] == MLConfig.__qualname__:
            model = torch.load(config["inputs"]["model"])
            mep2_batch = atoms_to_batch(mep2_atoms)
            mep2_es = model(mep2_batch)["energy"].detach().numpy().squeeze()

        interpolated_path2 = interpolate_path(*zip(*projected_mep2), mep2_es.tolist())
        ax.scatter(
            *zip(*projected_mep2), mep2_es, s=50, marker="o", c="r", depthshade=False
        )
        ax.plot(*zip(*interpolated_path2), c="g", linestyle="--")

    fig.savefig(config["outputs"]["mep.png"])

    plot = {}
    plot['surf']=  np.append(grid, np.expand_dims(grid_es, 1), axis=1).tolist()
    plot['mep']= np.append(projected_mep, np.expand_dims(mep_es, 1), axis = 1).tolist()
    plot['interpolated_mep'] = interpolated_path.tolist()

    json.dump(plot, open(config['outputs']['plot.json'], "w"))


def interpolate_path(*path, n=100):
    tck, _ = interpolate.splprep(
        path, s=0
    )  # pylint: disable=unbalanced-tuple-unpacking
    t = np.linspace(0, 1, n)
    x_out, y_out, z_out = interpolate.splev(t, tck)
    return np.array(list(zip(x_out, y_out, z_out)))


def find_lim(*pts, buf=0.2):
    array = np.array(pts)
    xs = array[:, 0]
    ys = array[:, 1]

    xmin, xmax = min(xs), max(xs)
    ymin, ymax = min(ys), max(ys)
    xbuf = buf * (xmax - xmin)
    ybuf = buf * (ymax - ymin)

    xlim = (xmin - xbuf, xmax + xbuf)
    ylim = (ymin - ybuf, ymax + ybuf)

    return xlim, ylim


def normalize(v):
    return v / np.linalg.norm(v)


def project(p, *basis_vectors):
    components = tuple(np.dot(p, b) for b in basis_vectors)
    projection = sum([c * b for c, b in zip(components, basis_vectors)])

    return components, projection


def to_original(projected_point, projection_vector, *us):
    point = -projection_vector
    for x, u in zip(projected_point, us):
        point += x * u

    return point


def grid_to_original(xs, ys, projection_vector, *us):
    return np.array(
        [to_original((x, y), projection_vector, *us) for x, y in zip(xs, ys)]
    )


def get_plane(p1, p2, p3):
    v1 = p1 - p2
    v2 = p2 - p3

    b1 = normalize(v1)
    v2_b1_parallel = np.dot(v2, b1) * b1
    v2_b1_perpendicular = v2 - v2_b1_parallel
    b2 = normalize(v2_b1_perpendicular)

    return b1, b2


class AtomFactory:
    def __init__(self, at):
        self.shape = at.positions.shape
        self.numbers = at.numbers

    def __call__(self, positions):
        positions = positions.reshape(self.shape)
        return Atoms(numbers=self.numbers, positions=positions)


class Plane:
    def __init__(self, p1, p2, p3):
        self.p1, self.p2, self.p3 = p1, p2, p3
        self.basis = get_plane(p1, p2, p3)

        self.projection_vector = self.get_projection_vector()

    def get_projection_vector(self):
        return sum(np.dot(self.p1, b) * b for b in self.basis) - self.p1

    def project2d(self, pts):
        if len(np.shape(pts)) == 1:
            return self._project2d(pts)
        return np.array([self._project2d(pt) for pt in pts])

    def _project2d(self, pt):
        return tuple(np.dot(pt, b) for b in self.basis)

    def transform_2d_to_nd(self, pts):
        if len(np.shape(pts)) == 1:
            return self._transform_2d_to_nd(pts)
        return np.array([self._transform_2d_to_nd(pt) for pt in pts])

    def _transform_2d_to_nd(self, pt):
        return sum(b * c for b, c in zip(self.basis, pt)) - self.projection_vector


def grid_from_limits(xlim, ylim, n=10):
    x_values = np.linspace(*xlim, n)
    y_values = np.linspace(*ylim, n)

    xx, yy = np.meshgrid(x_values, y_values)
    return np.array(list(zip(xx.flatten(), yy.flatten())))
