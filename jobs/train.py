import itertools

import matplotlib.pyplot as plt
import numpy as np
import torch
from pype import BaseConfig
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau

from train_ensemble import data, utils
from train_ensemble.early_stopper import EarlyStopper
from train_ensemble.evaluate import eval_model
from train_ensemble.pandas_logger import PandasLogger
from train_ensemble.rotating_loader import RotatingDataLoader


class Config(BaseConfig):
    script_path = "jobs/train.py"
    inputs = {"train_data", "model"}
    optional_inputs = {"val_data"}
    params = {
        "batch_size": 100,
        "lr": 1e-3,
        "logging_interval": 1000,
        "logging_steps": 100,
        "max_steps": 1000000,
        "max_epochs": float("inf"),
        "energy_key": "energy",
        "forces_key": "forces",
        "rho": 0.5,
        "scheduler_patience": 4000,
    }
    outputs = {
        "model_dir": "model",
        "train_logs": "train_logs.csv",
        "model": "model/best",
        "latest": "model/latest",
        "lr_plot": "learning_rate_curve.png",
        "training_curve": "training_curve.png",
        "min_val_loss": "min_val_loss.json",
        "energy_error.png": "energy_error.png",
        "forces_error.png": "forces_error.png",
        "device": "device",
    }


class LossFn(torch.nn.Module):
    def __init__(self, energy_key, forces_key, rho):
        super().__init__()
        self.energy_key = energy_key
        self.forces_key = forces_key
        self.rho = rho

    def forward(self, batch, result):

        diff_energy = batch[self.energy_key] - result[self.energy_key]
        err_sq_energy = torch.sum(diff_energy ** 2)

        diff_forces = batch[self.forces_key] - result[self.forces_key]
        err_sq_forces = torch.sum(diff_forces ** 2) / 3

        err_sq = self.rho * err_sq_energy + (1 - self.rho) * err_sq_forces

        return err_sq


def main(config, logger):
    logger.info("loading data ... ")
    open(config["outputs"]["device"], "w").write(utils.DEVICE)

    batch_size = config["params"]["batch_size"]
    train_data = config["inputs"]["train_data"]
    train_datasets = data.get_datasets(train_data)
    train_loaders = [
        data.get_dataloader(dataset, batch_size) for dataset in train_datasets
    ]
    train_loader = RotatingDataLoader(train_loaders)

    val_data = config["inputs"].get("val_data", None)
    if not val_data:
        val_data = train_data

    val_datasets = data.get_datasets(val_data)
    val_loaders = [data.get_dataloader(dataset, batch_size) for dataset in val_datasets]
    val_loader = RotatingDataLoader(val_loaders)

    model = torch.load(config["inputs"]["model"])
    model.to(utils.DEVICE)

    optimizer = Adam(model.parameters(), lr=config["params"]["lr"], amsgrad=True, weight_decay = 0.01 )

    patience = (
        config["params"]["scheduler_patience"] // config["params"]["logging_interval"]
    )
    scheduler = ReduceLROnPlateau(optimizer, patience=patience, factor=0.8)

    loss_fn = LossFn(
        energy_key=config["params"]["energy_key"],
        forces_key=config["params"]["forces_key"],
        rho=config["params"]["rho"],
    )

    early_stopper = EarlyStopper(model, config["output_dir"])
    pandas_logger = PandasLogger(
        config["outputs"]["train_logs"],
    )

    step = 0
    epoch = 0
    logger.info("starting training ...")
    # if config["inputs"]["previous_logs"]:
    #     pandas_logger = PandasLogger(
    #         config["outputs"]["train_logs"],
    #         previous_logs=config["inputs"]["previous_logs"],
    #     )
    #     epoch = pandas_logger.dataframe["epoch"].iloc[-1]
    #     step = pandas_logger.dataframe["step"].iloc[-1]
    #     optimizer = Adam(model.parameters(), lr=pandas_logger.dataframe["lr"].iloc[-1])

    running_loss = running_count = 0

    for epoch in itertools.count(epoch):
        for batch in train_loader:
            batch = utils.batch_to_device(batch, utils.DEVICE)

            step += 1
            optimizer.zero_grad()

            batch_size = config["params"]["batch_size"]
            result = model(batch)

            loss = loss_fn(
                batch=batch,
                result=result,
            )

            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            running_count += batch_size

            if np.isnan(running_loss):
                raise RuntimeError("nans appeared in loss")

            if (step % config["params"]["logging_interval"] == 0) or (
                step == config["params"]["max_steps"]
            ):
                train_loss = running_loss / running_count

                scheduler.step(train_loss)

                running_loss = 0
                running_count = 0

                val_loss, val_log = eval_model(
                    model=model,
                    dataloader=val_loader,
                    loss_fn=loss_fn,
                    logging_steps=config["params"]["logging_steps"],
                )

                log = {
                    "epoch": epoch,
                    "step": step,
                    "train_loss": train_loss,
                    "lr": optimizer.param_groups[0]["lr"],
                    **val_log,
                }

                pandas_logger.step(log)
                training_curve, lr_plot, energy_fig, forces_fig = plot_train(
                    pandas_logger.dataframe
                )
                training_curve.savefig(config["outputs"]["training_curve"])
                lr_plot.savefig(config["outputs"]["lr_plot"])
                energy_fig.savefig(config["outputs"]["energy_error.png"])
                forces_fig.savefig(config["outputs"]["forces_error.png"])
                plt.close("all")
                early_stopper.step(val_loss, log)
                logger.info(
                    f"step: {step}, epoch: {epoch}, train_loss: {train_loss:.2f}, val_loss: {val_loss:.2f}"
                )

            if (
                step >= config["params"]["max_steps"]
                or epoch >= config["params"]["max_epochs"]
            ):
                break

        else:
            continue
        break


def plot_train(df, running_mean=1):
    loss_fig, ax = plt.subplots()
    df["train_loss"] = df["train_loss"].rolling(window=running_mean).mean()
    df["val_loss"] = df["val_loss"].rolling(window=running_mean).mean()
    df.plot(y="val_loss", x="step", ax=ax, c="orange", label="Validation Loss")
    df.plot(y="train_loss", x="step", ax=ax, c="black", label="Train Loss")
    # ax.vlines(
    #     min_step,
    #     min_val_loss,
    #     max(max_val_loss, max_train_loss),
    #     linestyles="--",
    #     color="black",
    #     label="Best Validation: {:.2e}".format(min_val_loss),
    # )
    ax.legend()

    ax.grid()
    ax.set_yscale("log")
    ax.set_title("Training Curve")
    ax.set_ylabel("Loss")
    ax.set_xlabel("Step")

    # plot mae and rmse
    energy_fig, ax = plt.subplots()
    df.plot(y="energy_rmse", x="step", ax=ax, c="orange", label="Energy RMSE: eV")
    df.plot(y="energy_mae", x="step", ax=ax, c="black", label="Energy MAE: eV")

    ax.grid()
    ax.set_yscale("log")
    ax.set_title("Energy Error")
    ax.set_ylabel("Energy MAE/RMSE")
    ax.set_xlabel("Step")

    forces_fig, ax = plt.subplots()
    df.plot(y="forces_rmse", x="step", ax=ax, c="orange", label="Forces RMSE: eV/A")
    df.plot(y="forces_mae", x="step", ax=ax, c="black", label="Forces MAE: eV/A")

    ax.grid()
    ax.set_yscale("log")
    ax.set_title("Forces Error")
    ax.set_ylabel("Forces MAE/RMSE")
    ax.set_xlabel("Step")

    # Plot lr
    lr_fig, ax = plt.subplots()
    ax.set_yscale("log")
    ax.set_title("Learning Rate")
    ax.set_ylabel("Learning Rate")
    ax.set_xlabel("Step")
    ax.grid()
    df.plot(y="lr", x="step", ax=ax, label="Learning Rate", color="black")

    return loss_fig, lr_fig, energy_fig, forces_fig
