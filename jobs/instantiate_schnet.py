from pype import BaseConfig
import torch
import json

from train_ensemble.model.schnet import SchnetModel
from train_ensemble import utils


class Config(BaseConfig):
    script_path = "jobs/instantiate_schnet.py"
    params = {
        "num_interactions": 3,
        "hidden_state_size": 128,
        "cutoff": 5.0,
    }
    optional_inputs = {"normalization"}
    outputs = {
        "model": "model",
        "state_dict": "model.pth",
        "model_params": "model_params.json",
    }


def main(config, logger):
    target_mean = None
    target_stddev = None
    normalization = config["inputs"].get("normalization", None)
    if normalization:
        normalization = utils.load_json(normalization)
        target_mean = normalization["means"]["energy"]
        target_stddev = normalization["stddevs"]["energy"]
        logger.info(f"target mean: {target_mean}")
        logger.info(f"target var: {target_stddev}")

    schnet = SchnetModel(
        num_interactions=config["params"]["num_interactions"],
        hidden_state_size=config["params"]["hidden_state_size"],
        cutoff=config["params"]["cutoff"],
        target_mean=target_mean,
        target_stddev=target_stddev,
    )

    torch.save(schnet, config["outputs"]["model"])
    torch.save(schnet, config["outputs"]["state_dict"])
    json.dump(config["params"], open(config["outputs"]["model_params"], "w"))

    logger.info("instantiated schnet")
