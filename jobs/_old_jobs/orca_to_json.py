import json
import os

from autochem import OrcaResults
from pype import BaseConfig


class Config(BaseConfig):
    script_path = 'jobs/orca_to_json.py'
    inputs = {"orca_log"}
    outputs = {"energies": "energies.json"}


def main(config):
    orca_logs = config["inputs"]["orca_log"]
    # dirname = os.path.dirname(path_file)

    if not isinstance(config['inputs']['orca_log'], list):
        orca_logs = [orca_logs]

    energies = [OrcaResults(orca_log).total_energy for orca_log in orca_logs]

    json.dump(energies, open(config["outputs"]["energies"], "w"))
