import json

from pype import BaseConfig
import matplotlib.pyplot as plt

from train_ensemble import utils
import numpy as np


class Config(BaseConfig):
    script_path = "jobs/plot_meps.py"
    inputs = {
        "model_name",
        "ml_mep_rc",
        "ml_mep_ml_energy",
        "ml_mep_dft_energy",
        "dft_mep_rc",
        "dft_mep_ml_energy",
        "dft_mep_dft_energy",
    }
    optional_inputs={
        'ml_mep_ml_energy_std'
    }
    outputs ={'plots': 'plot.png'}


def main(config):
    model_name = config["inputs"]["model_name"]
    ml_mep_rc = json.load(open(config['inputs']['ml_mep_rc']))
    ml_mep_ml_energy = utils.translate(json.load(open(config['inputs']['ml_mep_ml_energy'])))
    ml_mep_dft_energy = utils.translate(json.load(open(config['inputs']['ml_mep_dft_energy'])))
    dft_mep_rc = json.load(open(config['inputs']['dft_mep_rc']))
    dft_mep_ml_energy = utils.translate(json.load(open(config['inputs']['dft_mep_ml_energy'])))
    dft_mep_dft_energy = utils.translate(json.load(open(config['inputs']['dft_mep_dft_energy'])))


    ml_energy_std = config['inputs']['ml_mep_ml_energy_std']
    if ml_energy_std:
        ml_energy_std = np.array(json.load(open(config['inputs']['ml_mep_ml_energy_std'])))
        top_error = ml_mep_ml_energy + ml_energy_std
        bot_error = ml_mep_ml_energy - ml_energy_std

    _, ax = plt.subplots(figsize=(15, 8))

    if ml_energy_std is not None: ax.fill_between(ml_mep_rc, top_error, bot_error, color='black', alpha=0.1)

    ax.plot(
        ml_mep_rc,
        ml_mep_ml_energy,
        label=f"{model_name} NEB - {model_name} Energy",
        color="black",
        linestyle="--",
    )

    ax.plot(
        ml_mep_rc,
        ml_mep_dft_energy,
        label=f"{model_name} NEB - DFT Energy",
        color="black",
    )
    ax.plot(
        dft_mep_rc,
        dft_mep_ml_energy,
        label=f"DFT NEB - {model_name} Energy",
        color="orange",
        linestyle="--",
    )
    ax.plot(
        dft_mep_rc, dft_mep_dft_energy, label="DFT NEB - DFT Energy", color="orange"
    )

    ax.set_title(f"Calculated MEP") # pylint: disable=f-string-without-interpolation
    ax.legend(loc="upper left")
    ax.set_xlabel("Path")
    ax.set_ylabel("Energy/eV")
    ax.grid()

    plt.savefig(config['outputs']['plots'])
