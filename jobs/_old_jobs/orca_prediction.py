
import json
import os

from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/orca_predction.py"
    inputs = {"xyz"}
    optional_inputs = {"transition_state"}
    params = {"cores": 1}
    outputs = {
        "orca_input": "orca/orca.inp",
        "orca_output": "orca/orca.out",
    }


def main(config, logger):
    cores = config["params"]["cores"]
    xyz = os.path.abspath(config["inputs"]["xyz"])
    os.makedirs(os.path.join(config["job_dir"], "output", "orca"), exist_ok=True)

    orca_input = get_orca_inp(cores, xyz)
    open(config["outputs"]["orca_input"], "w").write(orca_input)

    logger.info(f"Orca input:\n{orca_input}")

    logger.info("Starting orca run ... ")
    command = f"{config['inputs']['orca_binary']} {config['outputs']['orca_input']} > {config['outputs']['orca_output']}"
    os.system(command)

    logger.info("removing .tmp and .gbw files")

    __import__("pdb").set_trace() #TODO delme
    # energy = extract_energy(config["outputs"]["mep.xyz"])
    # json.dump(energy, open(config["outputs"]["energy.json"], 'w'))


def get_orca_inp(cores, xyz):
    inp = "! engrad wB97X 6-31G(d)\n"
    inp += f"%PAL NPROCS {cores} END\n"
    inp += f"* XYZfile 0 1 {xyz}\n"

    return inp
