import json
import os

from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/orca_neb.py"
    inputs = {"reactant", "product", "orca_binary"}
    optional_inputs = {"transition_state"}
    params = {"cores": 1}
    outputs = {
        "orca_input": "orca/orca.inp",
        "orca_output": "orca/orca.out",
        "mep.xyz": "orca/orca_MEP_trj.xyz",
        "energy.json": "energy.json",
    }


def main(config, logger):

    cores = config["params"]["cores"]
    orca_binary = config['inputs']['orca_binary']

    product = os.path.abspath(config["inputs"]["product"]).replace(os.path.expanduser('~'), '~')
    reactant = os.path.abspath(config["inputs"]["reactant"]).replace(os.path.expanduser('~'), '~')
    os.makedirs(os.path.join(config["job_dir"], "output", "orca"), exist_ok=True)

    transition_state = config["inputs"].get("transition_state", None)
    if transition_state:
        transition_state = os.path.abspath(config["inputs"]["transition_state"]).replace(os.path.expanduser('~'), '~')

    orca_input = get_orca_inp(cores, product, reactant, transition_state)

    open(config["outputs"]["orca_input"], "w").write(orca_input)
    logger.info(f"Orca input:\n{orca_input}")

    # logger.info("Starting orca run ... ")
    # command = f"{orca_binary} {config['outputs']['orca_input']} > {config['outputs']['orca_output']}"
    # os.system(command)

    # logger.info("removing .tmp and .gbw files")

    # energy = extract_energy(config["outputs"]["mep.xyz"])
    # json.dump(energy, open(config["outputs"]["energy.json"], 'w'))


def get_orca_inp(cores, product, reactant, transition_state=None):
    inp = "!wB97X 6-31G(d) NEB-CI\n"
    inp += f"%PAL NPROCS {cores} END\n"
    inp += f'%NEB NEB_END_XYZFILE "{product}"\n'

    if transition_state:
        inp += f'    NEB_TS_XYZFILE "{transition_state}"\n'

    inp += "    PREOPT_ENDS TRUE\n"
    inp += "END\n"
    inp += "\n"
    inp += f"* XYZfile 0 1 {reactant}\n"

    return inp


def extract_energy(atoms):
    xyz_string = open(atoms).read()
    energies = []

    for line in xyz_string.split("\n"):
        if line.startswith("Coordinates"):
            energy = float(line.split(" ")[-1])
            energies.append(energy)

    return energies
