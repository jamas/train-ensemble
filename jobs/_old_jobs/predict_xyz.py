import logging
import json

import ase
import torch
from pype import BaseConfig

from train_ensemble import calculator, utils

# orcablocks='%pal nprocs 16 end'


class ML_Config(BaseConfig):
    script_path = "jobs/predict_xyz.py"
    inputs = {"atoms_xyz", "model"}
    outputs = {"energy": "energies.json", 'energy_sigma': "energies_std.json"}
    params = {"units": "Hartree", "atoms_converter": "gnn", "uncertainty": False}


class ORCA_Config(BaseConfig):
    script_path = "jobs/predict_xyz.py"
    inputs = {"atoms_xyz"}
    optional_inputs = {'orca_binary'}
    outputs = {"energy": "energy.json", "orca_out": "orca_out/orcacalc"}
    params = {"units": "eV"}
    params = {"orca_prefix": "!wB97X 6-31G(d)", "cores": 1, "units": "eV"}


def main(config):
    atoms = utils.read_xyz(config["inputs"]["atoms_xyz"])

    if config["qualname"] == ML_Config.__qualname__:
        model = torch.load(config["inputs"]["model"])
        model.eval()
        get_calc = calculator.get_ml_calc_constructor(
            model, atoms_converter=config["params"]["atoms_converter"]
        )
        units = ase.units.Hartree

    if config["qualname"] == ORCA_Config.__qualname__:
        get_calc = calculator.get_orca_calc_constructor(
            cores=config["params"]["cores"],
            label=config["outputs"]["orca_out"],
            orcabinary=config["inputs"]["orca_binary"],
        )
        units = ase.units.eV

    if not isinstance(atoms, list):
        atoms = [atoms]

    energies = []
    for i, atom in enumerate(atoms):
        try:
            logging.info(
                f"calculating energy for geometry {i+1} out of {len(atoms)} ... "
            )
            atom.calc = get_calc()
            energy = atom.get_potential_energy() * units

            energies.append(energy)
        except (
            ase.calculators.calculator.PropertyNotImplementedError,
            ase.calculators.calculator.CalculationFailed,
        ):
            energies.append(float("inf"))
    utils.dump_json(energies, config["outputs"]["energies"])

    if config["params"].get("uncertainty", False):
        energies_std = [
            (atom_config.calc.get_property("energy_var")) ** 0.5 * units
            for atom_config in atoms
        ]
        json.dump(
            energies_std, open(config['outputs']['energies_std'], 'w')
        )
