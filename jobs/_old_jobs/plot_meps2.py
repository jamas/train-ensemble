import json
import os

from pype import BaseConfig
import matplotlib.pyplot as plt
import numpy as np

from train_ensemble import utils

COLORS = [u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#17becf']


# method
# dft_energy
# energy
# uncertainty
# rc


class Config(BaseConfig):
    script_path = "jobs/plot_meps2.py"
    inputs = {"summaries"}
    outputs = {"plots": "plot.png"}


def get_and_check(name, summary):
    if name not in summary:
        return None
    path = summary[name]
    if not os.path.exists(path):
        return None
    return json.load(open(path))

def main(config):
    _, ax = plt.subplots(figsize=(15, 8))
    for summary, color in zip(config["inputs"]["summaries"], COLORS):
        method = summary.get("method", None)

        rc = get_and_check('rc', summary)
        energy = get_and_check('energy', summary)
        uncertainty = get_and_check('uncertainty', summary)
        dft_energy = get_and_check('dft_energy', summary)

        if not rc:
            continue

        if uncertainty is not None:
            top_error = utils.translate(energy) + np.array(uncertainty)
            bot_error = utils.translate(energy) - np.array(uncertainty)
            ax.fill_between(rc, top_error, bot_error, color=color, alpha=0.1)

        if energy is not None:
            ax.plot(
                rc,
                utils.translate(energy),
                label=f"{method} NEB - {method} Energy",
                color=color,
                linestyle="--",
            )

        if dft_energy is not None:
            ax.plot(
                rc,
                utils.translate(dft_energy),
                label=f"{method} NEB - DFT Energy",
                color=color,
            )

    ax.set_title("MEPs")  # pylint: disable=f-string-without-interpolation
    ax.legend(loc="upper left")
    ax.set_xlabel("Path")
    ax.set_ylabel("Energy/eV")
    ax.grid()

    plt.savefig(config["outputs"]["plots"])
