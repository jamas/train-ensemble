import os
import json

import torch
import numpy as np
import schnetpack as spk

from train_ensemble import utils, data
from train_ensemble.model.schnet import get_schnet


def main(config):
    os.makedirs(config["output_paths"]["metrics"], exist_ok=True)
    energy_key = config["params"]["energy_key"]
    force_key = config["params"]["force_key"]
    model = get_schnet(energy_key, force_key)
    model.to(utils.DEVICE)

    state_dict = torch.load(
        os.path.join(config["inputs"]["model"], "best.pth"), map_location=utils.DEVICE
    )
    model.load_state_dict(state_dict)

    dataset = data.get_dataset(
        config["inputs"]["data"]["dbpath"],
        list(config["inputs"]["data"]["properties"].values()),
    )

    _, val_dataset, _ = data.read_split(
        dataset,
        config["inputs"]["split"],
    )

    val_loader = spk.AtomsLoader(val_dataset, batch_size=100)

    evaluation = {
        property_: {
            "target": np.array([]),
            "prediction": np.array([]),
            "error": np.array([]),
        }
        for property_ in [energy_key, force_key]
    }

    for batch in val_loader:
        batch = utils.batch_to_device(batch, utils.DEVICE)
        result = model(batch)
        for property_ in [energy_key, force_key]:

            target = batch[property_].cpu().detach().flatten()
            prediction = result[property_].cpu().detach().flatten()

            if property_ == force_key:
                # Unsqueeze on last dimension where the forces are, repeat mask 3 times along that
                # dimension and flatten to have a mask for forces
                force_mask = batch["_atom_mask"].unsqueeze(-1).repeat(1, 1, 3).flatten()

                prediction = torch.masked_select(prediction, force_mask.bool())
                target = torch.masked_select(target, force_mask.bool())

            error = abs(prediction - target)

            evaluation[property_]["target"] = np.append(
                evaluation[property_]["target"], target
            )

            evaluation[property_]["prediction"] = np.append(
                evaluation[property_]["prediction"], prediction
            )

            evaluation[property_]["error"] = np.append(
                evaluation[property_]["error"], error
            )

    json.dump(
        utils.cast_dict(evaluation, list),
        open(config["output_paths"]["evaluation"], "w"),
    )

    for property_ in [energy_key, force_key]:
        error = evaluation[property_]["error"]
        ME = me(error)
        RMSE = rmse(error)

        with open(
            os.path.join(config["output_paths"]["metrics"], property_ + "_rmse"), "w"
        ) as f:
            f.write(str(RMSE))

        with open(
            os.path.join(config["output_paths"]["metrics"], property_ + "_me"), "w"
        ) as f:
            f.write(str(ME))

def me(error):
    return error.mean()


def rmse(error):
    return (error ** 2).mean() ** 0.5


if __name__ == "__main__":
    utils.run(main)
