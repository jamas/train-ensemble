from ase.io.trajectory import TrajectoryWriter
from ase.io import write

from pype import BaseConfig

from train_ensemble import utils


class Config(BaseConfig):
    script_path = 'jobs/xyz_to_gif.py'
    inputs={'atoms_xyz'}
    outputs={'atoms_traj': 'atoms.traj', 'atoms_gif': 'atoms.gif'}


def main(config):
    __import__("pdb").set_trace() #TODO delme
    mep = utils.read_xyz(config['inputs']['atoms_xyz'])
    writer = TrajectoryWriter(config['outputs']['atoms_traj'], mode='a')
    for atoms in mep:
        writer.write(atoms)
    write(config['outputs']['atoms_gif'], images=mep, format='gif')
