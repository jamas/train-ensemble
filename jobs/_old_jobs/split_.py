"""
input directory must have a dataset.db

python prepare_data.py --input-dir ... --output-dir ... --config ...

should find data in the input-dir, create datasplits as specified in the config file and save the
datasplit in the output-dir
"""


import json
import ase.db

import numpy as np
import schnetpack as spk
import torch
from pype import BaseConfig

from train_ensemble import data, utils


class Config(BaseConfig):
    inputs = {"data"}
    params = {
        "n_train": None,
        "n_val": None,
        "energy_key": "energy",
        "forces_key": "forces",
        "random": True,
        "calculate_normalization": True,
    }
    outputs = {"split": "split.npz", "normalization": "normalization.json"}
    script_path = "jobs/split.py"


def main(config, logger):
    with ase.db.connect(config['inputs']['data']) as db:
        n_data = db.count()

    n_val = config["params"]["n_val"]
    n_train = config["params"]["n_train"]

    if not n_val and n_train:
        n_val = n_data - n_train

    if not n_train and n_val:
        n_train = n_data - n_val

    split_path = config["outputs"]["split"]
    dataset = data.get_dataset(config['inputs']['data'])
    __import__("pdb").set_trace() #TODO delme

    logger.info(
        f"Using {n_train} for train and {n_val} of {n_data} datapoints for train and "
        f"validation"
    )

    if not config["params"]["random"]:
        train_idx = np.array(range(n_train))
        val_idx = np.array(range(n_train, n_train + n_val))
        if n_train + n_val >= n_data:
            logger.info("using same data for test and validation")
            test_idx = val_idx
        else:
            test_idx = np.array(range(n_train + n_val, n_data))

        np.savez(split_path, train_idx=train_idx, val_idx=val_idx, test_idx=test_idx)

    else:
        train_dataset, _, _ = data.create_split(
            dataset=dataset,
            num_train=n_train,
            num_val=n_val,
            split_path=split_path,
        )

    train_dataset, _, _ = data.read_split(dataset, split_path)

    train_loader = spk.AtomsLoader(train_dataset, batch_size=100, num_workers=10)
    if config['params']['calculate_normalization']:
        logger.info("Calculating normalisation ... ")
        means, stddevs = train_loader.get_statistics(
            config["params"]["energy_key"],
            divide_by_atoms=True,
        )

        normalization = {"means": means, "stddevs": stddevs}
        logger.info("Normalisation done")

        json.dump(
            utils.cast_dict(normalization, torch.Tensor.tolist),
            open(config["outputs"]["normalization"], "w"),
        )
