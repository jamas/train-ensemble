import json

import torch
from pype import BaseConfig

from train_ensemble import utils
from train_ensemble.model import get_schnet


class Config(BaseConfig):
    script_path = "jobs/fix_schnet.py"
    inputs = {"state_dict"}
    optional_inputs = {"normalization"}
    outputs = {"model": "schnet.pt"}
    params = {
        "energy_key": "energy",
        "force_key": "forces",
    }


def main(config):
    normalization = json.load(open(config["inputs"]["normalization"]))

    if normalization:
        mean = torch.Tensor(normalization["means"][config["params"]["energy_key"]])
        stddev = torch.Tensor(normalization["stddevs"][config["params"]["energy_key"]])
    else:
        mean = torch.Tensor([0])
        stddev = torch.Tensor([1])

    schnet = get_schnet(
        energy_key=config["params"]["energy_key"],
        force_key=config["params"]["force_key"],
        mean=mean,
        stddev=stddev,
    )

    state_dict = torch.load(config["inputs"]["state_dict"], map_location="cpu")
    schnet.load_state_dict(state_dict)
    torch.save(schnet, config["outputs"]["model"])
