import ase.db
import matplotlib.pyplot as plt
import numpy as np
import tqdm
from numpy.linalg import norm
from pype import BaseConfig
from sklearn.cluster import KMeans

from train_ensemble.utils import distance


class Config(BaseConfig):
    script_path = "jobs/investigate_data_diversity.py"
    inputs = {"db"}
    outputs = {"plot": "plot"}


def main(config):
    dists = []
    with ase.db.connect(config["inputs"]["db"]) as db:
        atomss = list(db.select())
        positions = np.array([atoms.positions for atoms in atomss])
        shape = positions.shape
        reshape = (shape[0], np.prod(shape[1:]))
        positions = positions.reshape(reshape)

        kmeans = KMeans(n_clusters=1300, random_state=0).fit(positions)
        plt.hist(kmeans.labels_, bins=1300)
        plt.show()



# def plot_min_dist():
#         for i, at1 in tqdm.tqdm(enumerate(atoms)):
#             for j, at2 in enumerate(atoms):
#                 if i == j:
#                     continue
#                 dist = norm(at1 - at2)
#                 if dist < closest[i]:
#                     __import__("pdb").set_trace()  # TODO delme
#
#                 if dist < min_dist:
#                     min_dist = dist
#             dists += [min_dist]
#
#     plt.hist(dists, bins=1000, log=True)
#     plt.show()
