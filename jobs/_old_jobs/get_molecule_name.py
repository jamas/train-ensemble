from pype import BaseConfig
from train_ensemble import utils


class Config(BaseConfig):
    script_path = 'jobs/get_molecule_name.py'
    inputs={'atoms_xyz'}
    outputs={'molecule_name': 'molecule.txt'}


def main(config):

    atoms = utils.read_xyz(config['inputs']['atoms_xyz'])

    name = str(atoms[0].symbols)

    open(config['outputs']['molecule_name'], 'w').write(name)
