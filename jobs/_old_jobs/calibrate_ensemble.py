"""
Calculates a factor for the calibration of the dataset.
If split is in the configuration file, the dataset will be split and the validation part of the "\
       f"data will be used for calibration
"""
import json

import numpy as np
import torch
from pype import BaseConfig

from train_ensemble import data
from train_ensemble import utils


class Config(BaseConfig):
    script_path = "jobs/calibrate_ensemble.py"
    inputs = {
        "model",
        "data",
        "split",
    }
    params = {
        "max_datapoints": 5000,
        "energy_key": "energy",
        "forces_key": "forces",
        "model_type": "gnn",
        "batch_size": 100,
        "calibrate_force": False,
    }
    outputs = {"model": "model.pt", "scaling_factor": "scaling_factor.json"}


class SpkConfig(Config):
    pass


class GNNConfig(Config):
    pass


def main(config, logger):
    model = torch.load(config["inputs"]["model"])
    model.to(utils.DEVICE)
    model.eval()

    if config["qualname"] == GNNConfig.__qualname__:
        get_dataloaders = data.get_gnn_dataloaders

    elif config["qualname"] == SpkConfig.__qualname__:
        get_dataloaders = data.get_spk_dataloaders
    else:
        raise ValueError("You must use either the GNNConfig or the SpkConfig")

    _, val_loader, _ = get_dataloaders(
        config["inputs"]["data"],
        config["params"]["energy_key"],
        config["params"]["forces_key"],
        config["inputs"]["split"],
        int(config["params"]["batch_size"]),
    )

    count = 0
    cumulated_scaling = 0

    for i, batch in enumerate(val_loader):
        logger.info(
            f'{i * config["params"]["batch_size"]} / {config["params"]["max_datapoints"]}'
        )
        batch = utils.batch_to_device(batch, utils.DEVICE)
        result = model(batch)

        if config["params"]["calibrate_force"]:
            # cumulated_scaling += compute_scaling_factor(
            #     batch["forces"].detach().cpu().numpy().flatten(),
            #     result["forces"].detach().cpu().numpy().flatten(),
            #     result["forces_var"].detach().cpu().numpy().flatten(),
            # )

            for vals in zip(
                batch["forces"].flatten(),
                result["forces"].flatten(),
                result["forces_var"].flatten(),
            ):
                vals = [val.item() for val in vals]
                if not sum(vals) == 0 and not vals[2] == 0:
                    count += 1
                    cumulated_scaling += compute_scaling_factor(*vals)

        else:
            cumulated_scaling += compute_scaling_factor(
                batch["energy"].detach().cpu().numpy(),
                result["energy"].detach().cpu().numpy(),
                result["energy_var"].detach().cpu().numpy(),
            )
            count += config["params"]["batch_size"]

        if i * config["params"]["batch_size"] >= config["params"]["max_datapoints"]:
            break

    scaling_factor = cumulated_scaling / count
    model.set_scaling(scaling_factor)
    json.dump(scaling_factor, open(config["outputs"]["scaling_factor"], "w"))


    model.to('cpu')
    torch.save(model, config["outputs"]["model"])


def compute_scaling_factor(target, mean, var):
    return np.sum((target - mean) ** 2 / var)
