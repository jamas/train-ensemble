import sqlite3 as sl

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as st

from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/evaluate_uncertainty.py"
    inputs = {"db"}
    params = {"table": "energy"}
    outputs = {
        "confidence_curve": "confidence_curve.png",
        "reliability_curve": "reliability_curve.png",
        "calibration_curve": "calibration_curve.png",
    }


def main(config):
    con = sl.connect(config["inputs"]["db"])
    sql = f"SELECT * FROM {config['params']['table']};"

    df = pd.read_sql(sql, con)
    df["error"] = df["mean"] - df["target"]
    df["squared_error"] = df["error"] ** 2

    # Confidence Curve
    _, ax = plt.subplots()
    n_bins = 100
    confidence_curve = calculate_confidence_curve(df["var"], df["squared_error"])
    oracle_curve = calculate_confidence_curve(df["squared_error"], df["squared_error"])
    ax.plot(
        range(1, n_bins + 1), oracle_curve, linestyle="--", color="k", label="oracle"
    )
    ax.plot(
        range(1, n_bins + 1), confidence_curve, color="black", label="confidence_curve"
    )

    ax.set_title("Confidence Curve")
    ax.set_xlabel("bins / sorted after ensemble var")
    ax.set_ylabel("MSE")
    ax.grid()
    plt.legend()

    plt.savefig(config["outputs"]["confidence_curve"])

    # Reliability Curve
    _, ax = plt.subplots()
    n_bins = 10
    rmv, rmse, ence = reliability_curve(df["error"], df["var"], n_bins)

    lim = max(rmse.max(), rmv.max()) * 1.1

    ax.plot((0, 100), (0, 100), linestyle="--", color="black", label="oracle")
    ax.plot(rmv, rmse, marker="o", color="black", label="reliability curve")
    plt.plot([], [], " ", label=f"ENCE = {ence:.2f}")

    ax.set_xlim((0, lim))
    ax.set_ylim((0, lim))
    ax.set_title("Reliability diagram")
    ax.set_xlabel("RMV")
    ax.set_ylabel("RMSE")
    ax.legend(loc=2)
    ax.grid()

    plt.savefig(config["outputs"]["reliability_curve"])

    # Calibration Curve
    _, ax = plt.subplots()
    coverage, percentiles, squared_error = calibration_curve(
        df["target"], df["mean"], df["var"]
    )
    ax.plot(percentiles, coverage, color="black", label="calibration_curve")
    ax.plot((0, 1), (0, 1), color="black", linestyle="--", label="oracle")
    plt.plot([], [], " ", label=f"squared error = {squared_error:.2f}")

    ax.set_title("Calibration plot")
    ax.set_xlabel("Percentiles")
    ax.set_ylabel("Coverage")
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.legend(loc=4)
    ax.grid()
    plt.savefig(config["outputs"]["calibration_curve"])


### CALIBRATION CURVE
def calibration_curve(target, mean, var):
    std = np.sqrt(var)
    percentiles = np.linspace(0.01, 0.99, 99)
    coverage = np.zeros_like(percentiles)
    for i, percentile in enumerate(percentiles):
        threshold = st.norm.ppf(percentile, mean, std)
        coverage[i] = np.count_nonzero(target < threshold) / target.shape[0]

    squared_error = np.sum((percentiles - coverage) ** 2)
    return coverage, percentiles, squared_error


### RELIABILITY CURVE
def reliability_curve(errors, var, n_bins=10):
    bins = [int(i * len(errors) / n_bins) for i in range(n_bins + 1)]
    sort = np.argsort(var)
    rmv = np.array(
        [np.sqrt(np.mean(var[sort][bins[i] : bins[i + 1]])) for i in range(n_bins)]
    )
    rmse = np.array(
        [
            np.sqrt(np.mean(np.square(errors[sort][bins[i] : bins[i + 1]])))
            for i in range(n_bins)
        ]
    )
    ence = np.mean(np.abs(rmv - rmse) / rmv)
    return rmv, rmse, ence


### CONFIDENCE CURVE
def calculate_confidence_curve(predicted_var, squared_error, n_bins=100):
    bins = [int(i * len(squared_error) / n_bins) for i in range(n_bins + 1)]
    sorted_ = np.argsort(predicted_var)[::-1]
    conf = [np.mean(squared_error[sorted_][bins[i] :]) for i in range(n_bins)]
    return conf
