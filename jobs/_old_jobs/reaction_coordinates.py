import json
from rmsd import kabsch_rmsd

import numpy as np
from ase.io import iread
from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/reaction_coordinates.py"
    inputs = {"atoms_xyz"}
    outputs = {
        "reaction_coordinates": "reaction_coordinates.json",
        "normalized_reaction_coordinates": "normalized_reaction_coordinates.json",
    }


def main(config):
    atoms = list(iread(config["inputs"]["atoms_xyz"]))
    dists = np.array(
        [0] + [distance(atom_1, atom_2) for atom_1, atom_2 in zip(atoms, atoms[1:])]
    )

    reaction_coordinates = np.cumsum(dists)
    normalized_reaction_coordinates = reaction_coordinates / dists.sum()

    json.dump(
        list(reaction_coordinates), open(config["outputs"]["reaction_coordinates"], "w")
    )
    json.dump(
        list(normalized_reaction_coordinates),
        open(config["outputs"]["normalized_reaction_coordinates"], "w"),
    )


def distance(atom_1, atom_2):
    return kabsch_rmsd(atom_1.positions, atom_2.positions, translate=True)
