import json

import ase
from ase.io import iread, write
from pype import BaseConfig
import numpy as np


class Config(BaseConfig):
    script_path = "jobs/extract_energies_from_xyz.py"
    inputs = {"mep_xyz"}
    params = {"units": "Ha"}
    outputs = {
        "energies": "energies.json",
        "transition_state": "transitions_state.xyz",
        "activation_energy": "activation_energy.json",
    }


def main(config):
    atoms = list(iread(config["inputs"]["mep_xyz"]))
    xyz_string = open(config["inputs"]["mep_xyz"]).read()
    energies = []

    units = config["params"]["units"]
    assert units.lower() in ["Ha".lower(), "eV".lower()]

    units = getattr(ase.units, units)

    for line in xyz_string.split("\n"):
        if line.startswith("Coordinates"):
            energy = float(line.split(" ")[-1]) * units
            energies.append(energy)

    activation_energy = max(energies) - energies[0]

    ts_idx = np.argmax(energies)
    ts = atoms[ts_idx]

    write(config["outputs"]["transition_state"], ts)
    json.dump(energies, open(config["outputs"]["energies"], "w"))
    json.dump(activation_energy, open(config['outputs']['activation_energy'], 'w'))
