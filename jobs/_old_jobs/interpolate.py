import matplotlib.pyplot as plt
from pype import BaseConfig
from scipy.interpolate import CubicSpline
import numpy as np
import json
from ase.io import iread
from rmsd import kabsch_rmsd


class Config(BaseConfig):
    script_path = "jobs/interpolate.py"
    inputs = {"mep.json", "atoms.xyz"}
    outputs = {'rc.json': 'rc.json', "interp.json": "interp.json"}


def main(config, logger):
    atoms = list(iread(config["inputs"]["atoms.xyz"]))
    logger.info("calculating rc ...")
    dists = np.array(
        [0] + [distance(atom_1, atom_2) for atom_1, atom_2 in zip(atoms, atoms[1:])]
    )

    rc = np.cumsum(dists)
    json.dump(list(rc), open(config["outputs"]["rc.json"], "w"))

    mep = json.load(open(config["inputs"]["mep.json"]))

    logger.info("calculating spline interpolation ... ")
    spline_rc = np.linspace(0, rc[-1], 200)
    spline_mep = CubicSpline(rc, mep)(spline_rc)

    json.dump(
        {"rc": list(spline_rc), "mep": list(spline_mep)},
        open(config["outputs"]["interp.json"], 'w'),
    )


def distance(atom_1, atom_2):
    return kabsch_rmsd(atom_1.positions, atom_2.positions, translate=True)
