import json

import numpy as np
import torch
from pype import BaseConfig

from train_ensemble import data, utils


class Config(BaseConfig):
    script_path = "jobs/evaluate_model.py"
    inputs = {"data", "model"}
    params = {
        "batch_size": 100,
        "energy_key": "energy",
        "forces_key": "forces",
        "steps": 1000,
    }
    outputs = {"evaluation.json": "evaluation.json"}


def eval_model(model, dataloader, steps, **kwargs):
    running_count = 0

    target_energy = np.array([])
    model_energy = np.array([])
    target_forces = np.array([])
    model_forces = np.array([])

    for i, batch in enumerate(dataloader):
        if 'logger' in kwargs:
            kwargs['logger'].info(f'{i} / {steps}')


        batch = utils.batch_to_device(batch, utils.DEVICE)
        result = model(batch)

        target_energy = np.append(
            target_energy, batch["energy"].cpu().flatten().detach().numpy()
        )
        model_energy = np.append(
            model_energy, result["energy"].cpu().flatten().detach().numpy()
        )

        target_forces = np.append(
            target_forces, batch["forces"].cpu().flatten().detach().numpy()
        )
        model_forces = np.append(
            model_forces, result["forces"].cpu().flatten().detach().numpy()
        )

        running_count += dataloader.batch_size

        if i >= steps:
            break

    energy_errors = target_energy - model_energy
    energy_rmse = np.mean(energy_errors ** 2) ** 0.5
    energy_mae = np.mean(np.abs(energy_errors))

    forces_errors = target_forces - model_forces
    forces_rmse = np.mean(forces_errors ** 2) ** 0.5
    forces_mae = np.mean(np.abs(forces_errors))

    return {
        "energy_rmse": energy_rmse,
        "energy_mae": energy_mae,
        "forces_rmse": forces_rmse,
        "forces_mae": forces_mae,
    }


def main(config, logger):
    db_path = config["inputs"]["data"]
    model_path = config["inputs"]["model"]

    logger.info("loading dataset ... ")
    dataset = data.get_dataset(db_path)
    data_loader = data.get_dataloader(
        dataset, batch_size=config["params"]["batch_size"]
    )

    logger.info("loading model ... ")
    model = torch.load(model_path)

    logger.info("evaluation data set ... ")
    l = eval_model(model, data_loader, 300, logger=logger)

    json.dump(l, open(config["outputs"]["evaluation.json"], 'w'))
