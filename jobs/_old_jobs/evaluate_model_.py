import json
import os
import sqlite3 as sl

import pandas as pd
import torch
from pype import BaseConfig

from train_ensemble import data, utils


class Config(BaseConfig):
    script_path = "jobs/evaluate_model_.py"
    inputs = {"data", "split", "model"}
    params = {
        "batch_size": 100,
        "energy_key": "energy",
        "forces_key": "forces",
        "uncertainty": True,
    }
    outputs = {"db": "predictions.db", "rmse": "rmse.json", 'mae':'mae.json'}


class SpkConfig(Config):
    pass


class GNNConfig(Config):
    pass


def main(config):

    energy_key = config["params"]["energy_key"]
    forces_key = config["params"]["forces_key"]

    if os.path.exists(config["outputs"]["db"]):
        os.remove(config["outputs"]["db"])

    if config["qualname"] == GNNConfig.__qualname__:
        get_dataloaders = data.get_gnn_dataloaders

    elif config["qualname"] == SpkConfig.__qualname__:
        get_dataloaders = data.get_spk_dataloaders

    _, val_loader, _ = get_dataloaders(
        config["inputs"]["data"],
        energy_key,
        forces_key,
        config["inputs"]["split"],
        int(config["params"]["batch_size"]),
    )

    model = torch.load(config["inputs"]["model"])
    model.to(utils.DEVICE)
    model.eval()

    con = sl.connect(config["outputs"]["db"])

    if config["params"]["uncertainty"]:
        columns = "mean float, var float, target float"
        names = "mean, var, target"
        insert_sql = lambda key: f"INSERT INTO {key} ({names}) values(?, ?, ?)"
    else:
        columns = "mean float, target float"
        names = "mean,target"
        insert_sql = lambda key: f"INSERT INTO {key} ({names}) values(?, ?);"

    create_table_sql = (
        lambda key: f"""
        CREATE TABLE {key} (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            {columns}
        );
    """
    )

    con.execute(create_table_sql(energy_key))
    con.execute(create_table_sql(forces_key))

    for i, batch in enumerate(val_loader):
        print(i, "/", len(val_loader))
        batch = utils.batch_to_device(batch, utils.DEVICE)
        result = model(batch)

        with con:
            if config["params"]["uncertainty"]:
                energies = (
                    result[energy_key],
                    result[energy_key + "_var"],
                    batch[energy_key],
                )
                forces = (
                    result[forces_key].flatten(),
                    result[forces_key + "_var"].flatten(),
                    batch[forces_key].flatten(),
                )
            else:
                energies = (result[energy_key], batch[energy_key])
                forces = (
                    result[forces_key].flatten(),
                    batch[forces_key].flatten(),
                )

            for vals in zip(*energies):
                vals = [val.item() for val in vals]
                con.execute(insert_sql(energy_key), vals)

            for vals in zip(*forces):
                vals = [val.item() for val in vals]
                if not sum(vals) == 0:
                    con.execute(insert_sql(forces_key), vals)

    energy_sql = f"SELECT * FROM {energy_key};"
    energy_df = pd.read_sql(energy_sql, con)
    forces_sql = f"SELECT * FROM {forces_key};"
    forces_df = pd.read_sql(forces_sql, con)

    rmse = {
        "energy": (energy_df["mean"] - energy_df["target"]).pow(2).mean() ** 0.5,
        "forces": (forces_df["mean"] - forces_df["target"]).pow(2).mean() ** 0.5,
    }
    json.dump(rmse, open(config["outputs"]["rmse"], "w"))

    mae = {
        "energy": (energy_df["mean"] - energy_df["target"]).abs().mean(),
        "forces": (forces_df["mean"] - forces_df["target"]).abs().mean(),
    }
    json.dump(mae, open(config["outputs"]["mae"], "w"))
