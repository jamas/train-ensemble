import json
import matplotlib.pyplot as plt
import pandas as pd
from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/plot_train.py"
    inputs = {"train_logs"}
    params = {"running_mean": 1}
    outputs = {
        "lr_plot": "learning_rate_curve.png",
        "training_curve": "training_curve.png",
        "min_val_loss": "min_val_loss.json",
    }


def main(config):
    # Plot loss
    fig, ax = plt.subplots()
    df = pd.read_csv(config["inputs"]["train_logs"])

    min_val_loss = df["val_loss"].min()
    max_val_loss = df["val_loss"].max()
    max_train_loss = df["train_loss"].max()
    min_idx = df["val_loss"].argmin()
    min_step = df["step"][min_idx]

    df["train_loss"] = (
        df["train_loss"].rolling(window=config["params"]["running_mean"]).mean()
    )
    df["val_loss"] = df["val_loss"].rolling(window=config["params"]["running_mean"]).mean()
    df.plot(y="val_loss", x="step", ax=ax, c="orange", label='Validation Loss')
    df.plot(y="train_loss", x="step", ax=ax, c="black", label='Train Loss')
    ax.vlines(min_step, min_val_loss, max(max_val_loss, max_train_loss), linestyles='--', color='black', label='Best Validation: {:.2e}'.format(min_val_loss))
    ax.legend()

    ax.grid()
    ax.set_yscale("log")
    ax.set_title("Training Curve")
    ax.set_ylabel("Loss")
    ax.set_xlabel("Step")
    fig.savefig(config["outputs"]["training_curve"])
    json.dump(min_val_loss, open(config["outputs"]["min_val_loss"], "w"))

    # Plot lr
    fig, ax = plt.subplots()
    ax.set_yscale('log')
    ax.set_title('Learning Rate')
    ax.set_ylabel('Learning Rate')
    ax.set_xlabel('Step')
    ax.grid()
    df.plot(y='lr', x='step', ax=ax, label='Learning Rate', color="black")

    fig.savefig(config["outputs"]["lr_plot"])
