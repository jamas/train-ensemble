import torch
from ase import Atoms
from pype import BaseConfig
from schnetpack.data.atoms import AtomsConverter


class Config(BaseConfig):
    script_path = "jobs/change_units.py"
    inputs = {"model"}
    params = {
        "energy_from": "Hartree",
        "energy_to": "eV",
        "force_from": "Hartree / Aangstroem",
        "force_to": "eV / Aangstroem",
    }
    outputs = {"model": "model"}


def main(config):
    atoms = Atoms("H")
    atoms_converter = AtomsConverter()
    atoms = atoms_converter(atoms)

    model = torch.load(config["inputs"]["model"])
    for m in model.modules():
        m.requires_stress = False
    __import__("pdb").set_trace() #TODO delme
    result = model(atoms)
