import json

import numpy as np

from train_ensemble import utils
from pype import BaseConfig


class Config(BaseConfig):
    script_path = "jobs/path_distance.py"
    inputs = {"source_path_xyz", "target_path_xyz"}
    outputs = {
        "distance_forward": "distance_forward.json",
        "distance_reverse": "distance_reverse.json",
    }


def dist_line(x, a, b):
    n = (a - b) / np.linalg.norm(a - b)
    xa = x - a
    return np.linalg.norm(xa - np.dot(xa, n) * n)


def dist_lineseg(x, a, b):
    # First two conditionals check if the projection lies outside the line segment in which
    # case we will use the distance to the endpoints

    ax = a - x
    ab = a - b
    if np.dot(ax, ab) < 0:
        return np.linalg.norm(ax)

    bx = b - x
    ba = b - a
    if np.dot(bx, ba) < 0:
        return np.linalg.norm(bx)

    # Otherwise we calculate the distance to the infinite line
    return dist_line(x, a, b)


def dist_curve(x, curve):
    dists = []
    for p1, p2 in zip(curve, curve[1:]):
        dists.append(dist_lineseg(x, p1, p2))
    return min(dists)


def main(config):
    mep1_xyz = config["inputs"]["source_path_xyz"]
    mep2_xyz = config["inputs"]["target_path_xyz"]

    mep1 = utils.read_xyz(mep1_xyz)
    mep2 = utils.read_xyz(mep2_xyz)

    shift = mep1[0].positions.flatten() - mep2[0].positions.flatten()

    mep1_pos = [atom.positions.flatten() for atom in mep1]
    mep2_pos = [atom.positions.flatten() + shift for atom in mep2]

    dists_forward = [dist_curve(x, mep2_pos) for x in mep1_pos]
    dists_reverse = [dist_curve(x, mep1_pos) for x in mep2_pos]

    json.dump(dists_forward, open(config["outputs"]["distance_forward"], "w"))
    json.dump(dists_reverse, open(config["outputs"]["distance_reverse"], "w"))
