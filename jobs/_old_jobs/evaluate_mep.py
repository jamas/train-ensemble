from pype import BaseConfig
import json
import numpy as np


class Config(BaseConfig):
    script_path = "jobs/evaluate_mep.py"
    inputs = {
        "ml_mep_ml_energies",
        "ml_mep_dft_energies",
        "dft_mep_dft_energies",
        "dft_mep_ml_energies",
    }
    outputs = {"evaluation": "evaluation.json"}


def main(config):
    mlts_mle = max(translate(json.load(open(config["inputs"]["ml_mep_ml_energies"]))))
    mlts_dfte = max(translate(json.load(open(config["inputs"]["ml_mep_dft_energies"]))))
    dftts_dfte = max(
        translate(json.load(open(config["inputs"]["dft_mep_dft_energies"])))
    )
    dftts_mle = max(translate(json.load(open(config["inputs"]["dft_mep_ml_energies"]))))

    ml_error = mlts_mle - dftts_mle
    dft_error = mlts_dfte - dftts_dfte
    dft_ml_error = mlts_mle - dftts_dfte

    json.dump(
        {"dft_error": dft_error, "ml_error": ml_error, "dft_ml_error": dft_ml_error},
        open(config["outputs"]["evaluation"], "w"),
    )


def translate(lst):
    shift = lst[0]
    return np.array(lst) - shift
