import json
import os

import ase
import numpy as np
import torch
from ase.io import write
from ase.neb import NEB
from ase.optimize.fire import FIRE as QuasiNewton
from pype import BaseConfig

from train_ensemble import calculator, utils


class OutputMixin:
    outputs = {
        "energies": "energies.json",
        "energies_std": "energies_std.json",
        "activation_energy": "activation_energy.json",
        "activation_energy_std": "activation_energy_std.json",
        "mep_xyz": "mep.xyz",
        "transition_state_xyz": "transition_state.xyz",
        "converged": "converged.json",
        "fire_out_NEB": "fire_NEB.log",
        "fire_out_reactant": "fire_reactant.log",
        "fire_out_product": "fire_product.log",
        "summary": 'summary.json'
    }


class ML_Config(BaseConfig, OutputMixin):
    inputs = {"model"}
    optional_inputs = {"initial_path_xyz", "reactant", "product"}
    params = {"n_images": 10, "atoms_converter": None, "uncertainty": False}
    script_path = "jobs/neb_.py"


class DFTB_Config(BaseConfig, OutputMixin):
    optional_inputs = {
        "initial_path_xyz",
        "reactant",
        "product",
        "slako_dir",
        "dftbp_binary",
    }
    params = {"n_images": 10}
    script_path = "jobs/neb.py"


class ORCA_Config(BaseConfig, OutputMixin):
    optional_inputs = {
        "initial_path_xyz",
        "reactant",
        "product",
        "orca_binary",
    }
    params = {"n_images": 10, "orca_prefix": "!wB97X 6-31G(d)", "cores": 1}
    script_path = "jobs/neb.py"


def main(config):
    if config["qualname"] == ML_Config.__qualname__:
        model = torch.load(config["inputs"]["model"])
        model.eval()
        get_calc = calculator.get_ml_calc_constructor(
            model, atoms_converter=config["params"]["atoms_converter"], device=utils.DEVICE
        )
        units = ase.units.Hartree

    elif config["qualname"] == DFTB_Config.__qualname__:
        slako_dir = os.path.join(config["inputs"]["slako_dir"], "")
        dftbp_binary = config["inputs"]["dftbp_binary"]
        dftbp_binary = dftbp_binary if dftbp_binary else "dftb+"
        os.environ["ASE_DFTB_COMMAND"] = dftbp_binary + ' > /dev/null'
        get_calc = calculator.get_dftb_calc_constructor(slako_dir, "dftb_output")
        units = ase.units.eV

    elif config["qualname"] == ORCA_Config.__qualname__:
        get_calc = calculator.get_orca_calc_constructor(
            config["params"]["orca_prefix"],
            config["params"]["cores"],
            "orca_output",
        )
        units = ase.units.Hartree
        orca_binary = config["inputs"]["orca_binary"]
        os.environ["ASE_ORCA_COMMAND"] = orca_binary = (
            orca_binary if orca_binary else "orca"
        )

    if config["inputs"]["initial_path_xyz"]:
        atom_configs = utils.read_xyz(config["inputs"]["initial_path_xyz"])
        for atoms in atom_configs:
            atoms.calc = get_calc()
        reactant_converged, product_converged = relax_endpoints(
            atom_configs,
            units=units,
        )

    else:
        reactant = utils.read_xyz(config["inputs"]["reactant"])
        reactant.calc = get_calc()
        print("relaxing reactant")
        reactant_converged = relax_geometry(
            reactant,
            units=units,
            fmax=0.0004,
            steps=1500,
            logfile=config["outputs"]["fire_out_reactant"],
        )
        print(
            "reactant converged" if reactant_converged else "reactant didnt converge"
        )

        product = utils.read_xyz(config["inputs"]["product"])
        product.calc = get_calc()
        print("relaxing product")
        product_converged = relax_geometry(
            product,
            units=units,
            fmax=0.0004,
            steps=1500,
            logfile=config["outputs"]["fire_out_product"],
        )
        print(
            "product converged" if product_converged else "product didnt converge"
        )

        atom_configs = [
            reactant.copy() for i in range(config["params"]["n_images"] - 1)
        ] + [product]
        # These two lines needs to happen, otherwise all the n-1 images will have the same
        # coordinates
        band = NEB(atom_configs)
        band.interpolate("idpp")

    for atoms in atom_configs:
        atoms.calc = get_calc()

    band = NEB(atom_configs, climb=True)
    print("relaxing MEP")
    mep_converged = relax_geometry(
        band,
        fmax=0.0004,
        units=units,
        logfile=config["outputs"]["fire_out_NEB"],
        steps=2000,
    )
    print("MEP converged" if mep_converged else "MEP didnt converge")

    energies = [
        atom_config.get_potential_energy() * units for atom_config in atom_configs
    ]
    converged = {
        "product_converged": 1 if product_converged else 0,
        "reactant_converged": 1 if reactant_converged else 0,
        "mep_converged": 1 if mep_converged else 0,
    }
    ts_idx = np.argmax(energies)
    transition_state = atom_configs[ts_idx]
    activation_energy = energies[ts_idx] - energies[0]

    write(config["outputs"]["transition_state_xyz"], transition_state)
    json.dump(energies, open(config["outputs"]["energies"], "w"))
    json.dump(activation_energy, open(config["outputs"]["activation_energy"], "w"))
    json.dump(converged, open(config["outputs"]["converged"], "w"))
    write(config["outputs"]["mep_xyz"], atom_configs)

    summary = {
        'ts': transition_state,
        'energies': energies,
        'ts_energy': activation_energy,
        'converged': converged
    }

    if config["params"].get("uncertainty", False):
        energies_std = [
            (atom_config.calc.get_property("energy_var")) ** 0.5 * units
            for atom_config in atom_configs
        ]
        activation_energy_std = energies_std[ts_idx]
        json.dump(
            activation_energy_std, open(config["outputs"]["activation_energy_std"], "w")
        )
        json.dump(energies_std, open(config["outputs"]["energies_std"], "w"))
        summary['energy_uncertainty'] = energies_std
        summary['ts_energy_uncertainty'] = energies_std


    json.dump(summary, open(config['outputs']['summary']))


def relax_geometry(
    atoms, fmax=0.0001, steps=1000, units=ase.units.Hartree, logfile="/dev/null"
):
    """
    units it the units coming out of the calculator,
    fmax is the max accepted force in Hartree/AA
    """
    relax = QuasiNewton(atoms, logfile=logfile)
    return relax.run(fmax=fmax * ase.units.Hartree / units, steps=steps)


def relax_endpoints(atoms, fmax=0.0005, units=ase.units.Hartree):
    initial = atoms[0]
    final = atoms[-1]

    product_converged = relax_geometry(initial, fmax=fmax, units=units)
    reactant_converged = relax_geometry(final, fmax=fmax, units=units)
    return product_converged, reactant_converged
