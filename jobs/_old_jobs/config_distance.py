from ase.io import read
import json

from pype import BaseConfig
from rmsd import kabsch_rmsd


class Config(BaseConfig):
    script_path = "jobs/config_distance.py"
    inputs = {"atoms1", "atoms2"}
    outputs = {"distance": "distance.json"}


def main(config):
    atoms1 = read(config["inputs"]["atoms1"])
    atoms2 = read(config["inputs"]["atoms2"])

    dist = kabsch_rmsd(atoms1.positions, atoms2.positions, translate=True)
    json.dump(dist, open(config['outputs']['distance'], 'w'))
