import json
import os
import re

import matplotlib.pyplot as plt
import numpy as np
from ase.io import iread
from pype import BaseConfig
from tqdm import tqdm

from train_ensemble.utils import distance

default_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
blue = default_colors[0]
yellow = default_colors[1]


class Config(BaseConfig):
    script_path = "jobs/evaluate_meps.py"
    inputs = {"ml_summaries", "dft_summaries"}
    optional_inputs = {"mldft_summaries"}
    outputs = {
        "plots": "plots",
        "errors.json": "errors.json",
        "histogram_of_errors.png": "histogram_of_errors.png",
        "dft_ml_barriers.png": "dft_ml_barriers.png",
        "distance_error.png": "distance_error.png",
    }
    params={'evaluate_all': False}


def get_summary_paths(path):
    if path.endswith(".json"):
        return json.load(open(path))

    return open(path).read().split("\n")[:-1]


def get_summaries(paths):
    return {re.search(r"rxn\d+", path).group(): path for path in paths}


def main(config, logger):
    os.makedirs(config["outputs"]["plots"], exist_ok=True)

    ml_summary_paths = get_summary_paths(config["inputs"]["ml_summaries"])
    ml_summaries = get_summaries(ml_summary_paths)

    dft_summary_paths = get_summary_paths(config["inputs"]["dft_summaries"])
    dft_summaries = get_summaries(dft_summary_paths)

    if config["inputs"]["mldft_summaries"]:
        mldft_summary_paths = get_summary_paths(config["inputs"]["mldft_summaries"])
        mldft_summaries = get_summaries(mldft_summary_paths)
    else:
        mldft_summaries = ml_summaries

    common_keys = ml_summaries.keys() & dft_summaries.keys() & mldft_summaries.keys()
    barrier_diffs = []
    corrected_barrier_diffs = []
    dft_barriers = []
    ml_barriers = []
    corrected_ml_barriers = []
    dists = []

    logger.info("making plots ...")
    count = 0
    for rxn in tqdm(common_keys):
        try:
            ml_summary = ml_summaries[rxn]
            dft_summary = dft_summaries[rxn]
            mldft_summary = mldft_summaries[rxn]

            ml_summary = get_ts(json.load(open(ml_summary)), ml_summary)
            dft_summary = get_ts(json.load(open(dft_summary)), dft_summary)
            mldft_summary = get_ts(json.load(open(mldft_summary)), mldft_summary)

            if not config['params']['evaluate_all']:
                if not (ml_summary["converged"] and dft_summary["converged"]):
                    continue

                if ml_summary['step'] > 500:
                    continue

            count += 1

            d = distance(
                np.array(ml_summary["ts_config"]), np.array(dft_summary["ts_config"])
            )
            dists += [d]

            dft_barrier = dft_summary["barrier"]
            ml_barrier = ml_summary["barrier"]
            corrected_ml_barrier = mldft_summary["barrier"]

            dft_barriers += [dft_barrier]
            ml_barriers += [ml_barrier]
            corrected_ml_barriers += [corrected_ml_barrier]
            barrier_diffs += [ml_barrier - dft_barrier]
            corrected_barrier_diffs += [corrected_ml_barrier - dft_barrier]

            fig, ax = plt.subplots()
            ax.plot(
                ml_summary["path"], ml_summary["energy"], label="ML MEP", c=yellow
            )
            ax.plot(dft_summary["path"], dft_summary["energy"], label="DFT MEP", c=blue)
            ax.plot(mldft_summary["path"], mldft_summary["energy"], label="Corrected ML MEP", c=blue, linestyle='--')

            ax.set_xlabel("Reaction Coordinate [AA]")
            ax.set_ylabel("Energy [eV]")
            ax.legend()
            ax.grid()

            fig.savefig(os.path.join(config["outputs"]["plots"], rxn + ".png"))
            plt.close("all")

        except Exception as _:  # pylint: disable=bare-except
            # raise _
            ...

    # MAKE HISTOGRAM OF ERRORS PLOT
    fig, ax = plt.subplots()

    truncated_barrier_diffs = list(
        filter(lambda barrier: abs(barrier) < 1, barrier_diffs)
    )
    truncated_corrected_barrier_diffs = list(
        filter(
            lambda corrected_barrier: abs(corrected_barrier) < 1,
            corrected_barrier_diffs,
        )
    )

    ax.hist(
        truncated_barrier_diffs,
        bins=90,
        edgecolor="#169acf",
        linewidth=0.5,
        alpha=0.5,
        label='ML Barriers'
    )
    ax.hist(
        truncated_corrected_barrier_diffs,
        bins=90,
        edgecolor="#169acf",
        linewidth=0.5,
        alpha=0.5,
        label='Corrected Barriers'
    )
    ax.grid(alpha=0.2)
    ax.legend()

    ax.set_title("Histogram of Barrier Errors")
    ax.set_xlabel("Barrier Error / eV")
    ax.set_ylabel("Frequency")
    fig.savefig(config["outputs"]["histogram_of_errors.png"])

    max_ = max(dft_barriers)
    min_ = min(dft_barriers)

    # MAKE DFT VS ML BARRIER PLOT
    fig, ax = plt.subplots()
    ax.plot((min_, max_), (min_, max_), "k", linestyle="--")
    ax.scatter(dft_barriers, ml_barriers, s=5, alpha=0.5)
    # ax.scatter(dft_barriers, ml_barriers, s=5, alpha=0.5, label='ML')
    # ax.scatter(dft_barriers, corrected_ml_barriers, s=5, alpha=0.5, label='Corrected ML')
    ax.set_title("DFT barriers vs ML barriers")
    ax.set_xlabel("DFT Barrier / eV")
    ax.set_ylabel("ML Barrier / eV")
    ax.grid(alpha=0.2)
    ax.set_ylim(min_ - 0.5, max_ + 0.5)
    ax.set_xlim(min_ - 0.5, max_ + 0.5)
    # ax.legend()
    fig.savefig(config["outputs"]["dft_ml_barriers.png"])

    # MAKE DISTANCE VS BARRIER DIFF PLOT
    fig, ax = plt.subplots()
    ax.hist(
        dists,
        bins=90,
        edgecolor="#169acf",
        linewidth=0.5,
        alpha=0.5,
        label='ML Barriers'
    )
    # ax.scatter(dists, barrier_diffs, s=5)
    # ax.plot(dists, 1 * np.array(dists), "r", linestyle="--")
    # ax.set_title("Barrier error vs Configurational distance")
    # ax.set_xlabel("Configurational distance Å")
    # ax.set_ylabel("Error / eV")
    # ax.legend()
    fig.savefig(config["outputs"]["distance_error.png"])


    # corrected - ml
    fig, ax = plt.subplots()
    ax.hist(
        np.array(corrected_ml_barriers) - np.array(ml_barriers),
        bins=90,
        edgecolor="#169acf",
        linewidth=0.5,
        alpha=0.5,
        label='ML Barriers'
    )
    # ax.scatter(dists, barrier_diffs, s=5)
    # ax.plot(dists, 1 * np.array(dists), "r", linestyle="--")
    # ax.set_title("Barrier error vs Configurational distance")
    # ax.set_xlabel("Configurational distance Å")
    # ax.set_ylabel("Error / eV")
    # ax.legend()
    fig.savefig(os.path.join(config["output_dir"], 'potential_error.png'))

    # Calculate RMSE and MAE
    rmse = np.mean(np.array(barrier_diffs) ** 2) ** 0.5
    mae = np.mean(np.abs(np.array(barrier_diffs)))

    corrected_rmse = np.mean(np.array(corrected_barrier_diffs) ** 2) ** 0.5
    corrected_mae = np.mean(np.abs(np.array(corrected_barrier_diffs)))

    json.dump(
        {
            "mae": mae,
            "rmse": rmse,
            "count": count,
            "corrected_mae": corrected_mae,
            "corrected_rmse": corrected_rmse,
        },
        open(config["outputs"]["errors.json"], "w"),
    )


def get_ts(summary, path):
    if "ts_config" in summary:
        return summary
    mep_path = path.replace("summary.json", "mep.xyz")
    atoms = list(iread(mep_path))
    ts = np.argmax([at.get_potential_energy() for at in atoms])
    ts_config = atoms[ts].positions.tolist()
    summary["ts_config"] = ts_config
    return summary
