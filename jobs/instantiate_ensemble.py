from train_ensemble.model.ensemble import Ensemble
from pype import BaseConfig
import torch


class Config(BaseConfig):
    script_path = "jobs/instantiate_ensemble.py"
    inputs = {"models"}
    params = {'energy_key': 'energy', 'force_key': 'forces'}
    outputs = {"model": "model", "state_dict": "model.pt"}


def main(config):
    paths = config["inputs"]["models"]
    ensemble = Ensemble(
        paths,
        energy_key=config["params"]["energy_key"],
        force_key=config["params"]["force_key"],
    )
    torch.save(ensemble, config["outputs"]["model"])
    torch.save(ensemble.state_dict(), config["outputs"]["state_dict"])
