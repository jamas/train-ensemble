from pype import BaseConfig
import torch
import json

from train_ensemble.model.painn import PainnModel
from train_ensemble import utils


class Config(BaseConfig):
    script_path = "jobs/instantiate_painn.py"
    params = {
        "num_interactions": 3,
        "hidden_state_size": 128,
        "cutoff": 5.0,
        "direct_force_output": False,
    }
    optional_inputs = {"normalization"}
    outputs = {
        "model": "model.pt",
        "state_dict": "model.pth",
        "model_params": "model_params.json",
    }


def main(config, logger):
    target_mean = None
    target_stddev = None
    normalization = config["inputs"].get("normalization", None)
    if normalization:
        normalization = utils.load_json(normalization)
        target_mean = normalization["means"]["energy"]
        target_stddev = normalization["stddevs"]["energy"]
        logger.info(f"target mean: {target_mean}")
        logger.info(f"target mean: {target_mean}")

    painn = PainnModel(
        num_interactions=config["params"]["num_interactions"],
        hidden_state_size=config["params"]["hidden_state_size"],
        cutoff=config["params"]["cutoff"],
        target_mean=target_mean,
        target_stddev=target_stddev,
        direct_force_output=config["params"]["direct_force_output"],
    )

    torch.save(painn, config["outputs"]["model"])
    torch.save(painn, config["outputs"]["state_dict"])
    json.dump(config["params"], open(config["outputs"]["model_params"], "w"))

    logger.info("instantiated painn")
