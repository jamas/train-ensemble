"""
input directory must have a dataset.db

python prepare_data.py --input-dir ... --output-dir ... --config ...

should find data in the input-dir, create datasplits as specified in the config file and save the
datasplit in the output-dir
"""

import json
import random

import numpy as np
import torch
from pype import BaseConfig

from train_ensemble import data


class Config(BaseConfig):
    inputs = {"data"}
    optional_inputs = {"index"}
    params = {
        "val_part": 0.05,
        "test_part": 0.05,
        "n_val": None,
        "energy_key": "energy",
        "forces_key": "forces",
        "random": True,
        "criterion": None
    }
    outputs = {
        "train_idx.json": "train_idx.json",
        "val_idx.json": "val_idx.json",
        "test_idx.json": "test_idx.json",
        "normalization.json": "normalization.json",
        "train_molecules.json": "train_molecules.json",
        "val_molecules.json": "val_molecules.json",
        "test_molecules.json": "test_molecules.json",
    }
    script_path = "jobs/split.py"


def get_subset(superset, n_keys, condition):
    if not condition:
        condition = lambda _: True

    subset = set()

    while len(subset) < n_keys:
        missing = n_keys - len(subset)
        proposed_keys = set(random.sample(superset, missing))

        for key in proposed_keys:
            if condition(key):
                subset.add(key)

    return subset


def main(config, logger):
    dataset = data.get_dataset(config["inputs"]["data"])
    n_data = len(dataset)
    all_heavy = lambda key: ("N" in key and "C" in key and "O" in key and "F" not in key)

    criterions = {'all_heavy': all_heavy}
    criterion = criterions.get(config['params']['criterion'], None)

    if config["inputs"]["index"]:
        index = json.load(open(config["inputs"]["index"]))
        train_keys = index.keys()
        n_keys = len(train_keys)
        val_keys = set()

        n_val_keys = int(config["params"]["val_part"] * n_keys)
        n_test_keys = int(config["params"]["test_part"] * n_keys)

        val_keys = get_subset(train_keys, n_val_keys, criterion)
        train_keys = train_keys - val_keys

        test_keys = get_subset(train_keys, n_test_keys, criterion)
        train_keys = train_keys - test_keys

        val_idx = []
        train_idx = []
        test_idx = []

        for key in train_keys:
            train_idx += index[key]

        for key in val_keys:
            val_idx += index[key]

        for key in test_keys:
            test_idx += index[key]

        json.dump(
            list(train_keys), open(config["outputs"]["train_molecules.json"], "w")
        )
        json.dump(list(val_keys), open(config["outputs"]["val_molecules.json"], "w"))
        json.dump(list(test_keys), open(config["outputs"]["test_molecules.json"], "w"))

        n_train = len(train_idx)
        n_val = len(val_idx)
        n_test = len(test_idx)

    else:
        n_val = int(config["params"]["val_part"] * n_data)
        n_test = int(config["params"]["test_part"] * n_data)
        n_train = n_data - n_val - n_test

        idx = list(range(n_data))
        np.random.shuffle(idx)

        train_idx = idx[:n_train]
        val_idx = idx[n_train : n_train + n_val]
        test_idx = idx[n_train + n_val : n_train + n_val + n_test]

    train_data = torch.utils.data.Subset(dataset, train_idx)
    dataloader = data.get_dataloader(train_data, batch_size=100)


    energy_pr_atom = np.array([])

    logger.info(
        f"using {n_train} for training, {n_val} for validation and {n_test} for testing of {n_data} ... "
    )

    json.dump(train_idx, open(config["outputs"]["train_idx.json"], "w"))
    json.dump(val_idx, open(config["outputs"]["val_idx.json"], "w"))
    json.dump(test_idx, open(config["outputs"]["test_idx.json"], "w"))

    logger.info("saved splits, now calculating normalization")

    logger.info("calculating normalization")

    for i, batch in enumerate(dataloader):
        nodes = np.count_nonzero(batch["nodes"], axis=1)
        energy = batch["energy"].numpy().squeeze()
        energy_pr_atom = np.append(energy_pr_atom, np.divide(energy, nodes))

        if i % 100 == 0:
            logger.info(f"{i / 10}% done")

        if i > 1000:
            break

    normalization = {
        "means": {"energy": energy_pr_atom.mean()},
        "stddevs": {"energy": energy_pr_atom.std()},
    }

    json.dump(normalization, open(config["outputs"]["normalization.json"], "w"))
