import io
import json
import os
import shutil
import sys
import threading
import uuid

import ase.db
import matplotlib.pyplot as plt
import numpy as np
import torch
from ase.io import write
from ase.neb import NEB, NEBOptimizer
from ase.optimize.bfgs import BFGS

# from ase.optimize.fire import FIRE
from pype import BaseConfig
from rmsd import kabsch_rmsd

from train_ensemble import calculator, utils


class OutputMixin:
    script_path = "jobs/neb.py"
    outputs = {
        "energy": "energy.json",
        "energy_sigma": "energy_sigma.json",
        "mep.xyz": "mep.xyz",
        "orcadir": "orca",
        "dftbdir": "dftb",
        "converged": "converged",
        "neb.db": "neb.db",
        "neb.log": "neb.log",
        "reactant.log": "reactant.log",
        "product.log": "product.log",
        "mep.png": "mep.png",
        "summary.json": "summary.json",
        "fmaxs.json": "fmaxs.json",
        "barriers.json": "barriers.json",
        "neb.png": "neb.png",
        "rxn.gif": "rxn.gif",
    }


shared_params = {
    "n_images": 10,
    "parallel": True,
    "fmax": 0.05,
    "optimizer": "BFGS",
    "maxstep": 0.05,
    "optimizer_steps": 300,
    "uncertainty": False,
    "write_db": True,
}


class GNNConfig(BaseConfig, OutputMixin):
    inputs = {"reactant", "product", "model"}
    optional_inputs = {"transition_state"}
    params = {
        **shared_params,
        "optimizer_steps": 1000,
    }


class SPKConfig(BaseConfig, OutputMixin):
    inputs = {"reactant", "product", "model"}
    optional_inputs = {"transition_state"}
    params = {
        **shared_params,
        "optimizer_steps": 1000,
    }


class ORCAConfig(BaseConfig, OutputMixin):
    inputs = {"reactant", "product", "orcabinary"}
    optional_inputs = {"transition_state"}
    params = {**shared_params, "cores": 1, "scratch": None, "optimizer_steps": 500}


class DFTBConfig(BaseConfig, OutputMixin):
    inputs = {"reactant", "product", "dftb_binary", "slako_dir"}
    optional_inputs = {"transition_state"}
    params = {**shared_params, "scratch": None, "optimizer_steps": 500}


# OPTIMIZERS = {"BFGS": BFGS, "FIRE": FIRE}


ATOM_CONVERTERS = {
    GNNConfig.__qualname__: "gnn",
    SPKConfig.__qualname__: "spk",
}


def main(config, logger): # pylint: disable=too-many-branches
    product = utils.read_xyz(config["inputs"]["product"])
    reactant = utils.read_xyz(config["inputs"]["reactant"])

    atom_configs = [
        reactant.copy() for i in range(config["params"]["n_images"] - 1)
    ] + [product]

    neb_log = LogFile(open(config["outputs"]["neb.log"], "w").detach())
    neb_log.logger = logger  # pylint: disable=attribute-defined-outside-init
    reactant_log = LogFile(open(config["outputs"]["reactant.log"], "w").detach())
    reactant_log.logger = logger  # pylint: disable=attribute-defined-outside-init
    product_log = LogFile(open(config["outputs"]["product.log"], "w").detach())
    product_log.logger = logger  # pylint: disable=attribute-defined-outside-init

    def sync_scratch():
        if config["params"].get("scratch", ""):
            shutil.copytree(orcadir, config["outputs"]["orcadir"], dirs_exist_ok=True)

    def clean_scratch():
        if config["params"].get("scratch", ""):
            shutil.rmtree(orcadir)

    #### ORCA CALCULATOR ####
    if config["qualname"] == ORCAConfig.__qualname__:
        if config["params"]["scratch"]:
            orcadir = os.path.join(
                config["params"]["scratch"], uuid.uuid4().hex, "orca"
            )
            logger.info(f"orca working in scratch dir {orcadir}")
        else:
            orcadir = config["outputs"]["orcadir"]

        calc_factory = calculator.OrcaCalcFactory(
            orcadir=orcadir,
            orcabinary=config["inputs"]["orcabinary"],
        )

        endpoint_cores = int(config["params"]["cores"] / 2)
        intermediate_cores = int(
            config["params"]["cores"] / (config["params"]["n_images"] - 2)
        )
        logger.info(f"relaxing endpoints using {endpoint_cores} cores each.")
        logger.info(
            f"relaxing intermediate images using {intermediate_cores} core(s) each."
        )

        atom_configs[0].calc = calc_factory(cores=endpoint_cores, label="reactant")
        atom_configs[-1].calc = calc_factory(cores=endpoint_cores, label="product")

        for i, atom_config in enumerate(atom_configs[1:-1]):
            atom_config.calc = calc_factory(cores=intermediate_cores, label=f"image{i}")

    if config["qualname"] == DFTBConfig.__qualname__:
        if config["params"]["scratch"]:
            dftbdir = os.path.join(
                config["params"]["scratch"], uuid.uuid4().hex, "dftb"
            )
            logger.info(f"dftb working in scratch dir {dftbdir}")
        else:
            dftbdir = config["outputs"]["dftbdir"]

        for i, atom_config in enumerate(atom_configs):
            atom_config.calc = calculator.get_dftb_calculator(
                slako_dir=config["inputs"]["slako_dir"],
                dftb_binary=config["inputs"]["dftb_binary"],
                label = os.path.join(dftbdir, str(i), 'dftb')
            )


    #### ML CALCULATORS ####
    if config["qualname"] in [GNNConfig.__qualname__, SPKConfig.__qualname__]:
        model = torch.load(config["inputs"]["model"])
        model.eval()

        atom_configs[0].calc = calculator.MLCalculator(model, device=utils.DEVICE)
        atom_configs[-1].calc = calculator.MLCalculator(model, device=utils.DEVICE)

        batch_handler = calculator.BatchHandler(atom_configs[1:-1])
        for atoms in batch_handler.atoms:
            atoms.calc = calculator.MLCalculator(
                model, device=utils.DEVICE, batch_handler=batch_handler
            )

    # optimizer = OPTIMIZERS[config["params"]["optimizer"]]
    optimizer = NEBOptimizer
    logger.info("relaxing endpoints ... ")
    relax_product = BFGS(atom_configs[0], logfile=product_log)
    relax_reactant = BFGS(atom_configs[-1], logfile=reactant_log)

    if config["params"]["parallel"]:
        threads = [
            threading.Thread(
                target=relax.run,
                kwargs={
                    "fmax": config["params"]["fmax"],
                    "steps": config["params"]["optimizer_steps"],
                },
            )
            for relax in [relax_product, relax_reactant]
        ]

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    else:
        relax_product.run(
            steps=config["params"]["optimizer_steps"], fmax=config["params"]["fmax"]
        )
        relax_reactant.run(
            steps=config["params"]["optimizer_steps"], fmax=config["params"]["fmax"]
        )

    logger.info("interpolating path ... ")

    interpolate_band(atom_configs, config["inputs"]["transition_state"])

    logger.info("relaxing NEB ... ")
    neb = NEB(atom_configs, climb=True, parallel=config["params"]["parallel"])

    def clean_up():
        sync_scratch()
        early_stopper.save()
        write(config["outputs"]["mep.xyz"], atom_configs)
        clean_scratch()

    def check_calculations():
        missing_calculations = []
        for i, image in enumerate(neb.images[1:-1]):
            if {"forces", "energy"} - image.calc.results.keys():
                missing_calculations.append(i)

        if missing_calculations:
            clean_up()
            raise ValueError(f"missing calculation for image(s) {missing_calculations}")

    early_stopper = EarlyStopper(
        neb.images,
        name=config["job_id"],
        summary_fp=config["outputs"]["summary.json"],
        barriers_fp=config["outputs"]["barriers.json"],
        fmax_fp=config["outputs"]["fmaxs.json"],
        mep_plot_fp=config["outputs"]["mep.png"],
        neb_plot_fp=config["outputs"]["neb.png"],
        mep_xyz_fp=config["outputs"]["mep.xyz"],
        gif_fp=config["outputs"]["rxn.gif"],
        uncertainty=config["params"]["uncertainty"],
    )

    relax_neb = optimizer(neb, logfile=neb_log)
    relax_neb.attach(check_calculations)
    relax_neb.attach(early_stopper.step)
    if config["params"]["write_db"]:
        db_writer = DBWriter(config["outputs"]["neb.db"], atom_configs)
        relax_neb.attach(db_writer.write)

    neb_converged = relax_neb.run(
        fmax=config["params"]["fmax"] * 2, steps=config["params"]["optimizer_steps"]
    )

    steps_taken = relax_neb.nsteps


    converged=False
    if neb_converged:
        logger.info("turn on CI-NEB ...")
        neb.climb = True
        early_stopper.start_climbing_image()
        converged = relax_neb.run(
            fmax=config["params"]["fmax"],
            steps=config["params"]["optimizer_steps"] - steps_taken,
        )

    early_stopper.set_converged(converged)
    clean_up()

    if early_stopper.converged:
        open(config["outputs"]["converged"], "w")
    if config["params"].get("uncertainty", False):
        energy_sigma = [
            (atom_config.calc.get_property("energy_var"))
            for atom_config in atom_configs
        ]
        json.dump(energy_sigma, open(config["outputs"]["energy_sigma"], "w"))


def interpolate_band(atom_configs, transition_state=None):
    if transition_state:
        transition_state = utils.read_xyz(transition_state)
        ts_positions = transition_state.get_positions()
        middle_idx = len(atom_configs) // 2
        atom_configs[middle_idx].set_positions(ts_positions)
        first_band = NEB(atom_configs[: middle_idx + 1])
        second_band = NEB(atom_configs[middle_idx:])
        first_band.interpolate("idpp")
        second_band.interpolate("idpp")
    else:
        band = NEB(atom_configs)
        band.interpolate("idpp")

    return atom_configs


class EarlyStopper:
    def __init__(
        self,
        images,
        name,
        uncertainty,
        summary_fp,
        barriers_fp,
        fmax_fp,
        mep_plot_fp,
        neb_plot_fp,
        mep_xyz_fp,
        gif_fp,
    ):
        self.images = images
        self.fmaxs = []
        self.barriers = []
        self.summary_fp = summary_fp
        self.barriers_fp = barriers_fp
        self.fmaxs_fp = fmax_fp
        self.mep_plot_fp = mep_plot_fp
        self.neb_plot_fp = neb_plot_fp
        self.mep_xyz_fp = mep_xyz_fp
        self.gif_fp = gif_fp

        self.summary = {}
        self.best_barrier = float("inf")
        self.best_fmax = float("inf")
        self.neb_tools = ase.neb.NEBTools(images)
        self.climb = False
        self.nstep = 0
        self.name = name
        self.uncertainty = uncertainty
        self.converged = False

    def step(self):
        self.nstep += 1
        barrier = self.neb_tools.get_barrier()[0]
        fmax = self.neb_tools.get_fmax()

        self.fmaxs.append(fmax)
        self.barriers.append(barrier)

        if barrier < self.best_barrier and fmax < 100 * self.best_fmax:
            self.best_barrier = barrier
            self.best_fmax = fmax

            self.summary = (  # pylint: disable=attribute-defined-outside-init
                self.get_summary()
            )

    def set_converged(self, converged):
        self.converged = converged  # pylint: disable=attribute-defined-outside-init

    def start_climbing_image(self):
        self.climb = len(self.barriers)

    def get_summary(self):
        fit = self.neb_tools.get_fit()
        barrier = self.barriers[-1]
        fmax = self.fmaxs[-1]

        ts_config, ts_forces = get_ts_config_and_forces(self.images)
        ts_force_norm = np.linalg.norm(ts_forces)
        summary = {
            "barrier": barrier,
            "fmax": fmax,
            "converged": self.converged,
            "CI": self.climb,
            "step": self.nstep,
            "symbols": str(self.images[0].symbols),
            "name": self.name,
            "energy": fit.fit_energies.tolist(),
            "path": fit.fit_path.tolist(),
            "ts_config": ts_config,
            "ts_forces": ts_forces,
            "ts_force_norm": ts_force_norm,
        }

        if self.uncertainty:
            uncertainty = [image.calc.results["energy_var"] for image in self.images]
            summary["uncertainty"] = uncertainty
            summary["max_uncertainty"] = max(uncertainty)

        return summary

    def save(self):
        if self.summary:
            self.summary["converged"] = bool(self.converged)
            json.dump(self.summary, open(self.summary_fp, "w"))
        json.dump(self.barriers, open(self.barriers_fp, "w"))
        json.dump(self.fmaxs, open(self.fmaxs_fp, "w"))
        write(self.mep_xyz_fp, self.images)

        fig = neb_fig(self.barriers, self.fmaxs, self.climb)
        fig.savefig(self.neb_plot_fp)
        plt.close(fig)
        write_gif(self.images, self.gif_fp)

        if self.summary:
            fig = mep_fig(self.summary["path"], self.summary["energy"])
            fig.savefig(self.mep_plot_fp)

        plt.close(fig="all")


def get_ts_config_and_forces(images):
    energies = [image.calc.get_potential_energy() for image in images]
    ts_idx = np.argmax(energies)
    ts = images[ts_idx]

    ts_forces = ts.get_forces().tolist()
    ts_config = ts.positions.tolist()

    return ts_config, ts_forces


def neb_fig(barriers, fmaxs, climb_started):
    fig, ax = plt.subplots()
    ax.plot(barriers, label="Barrier")
    ax.set_ylabel("Barrier [eV]")
    ax.set_xlabel("Step")
    if climb_started:
        ax.axvline(climb_started, linestyle="--", label="Start CI")

    ax2 = ax.twinx()
    ax2.plot(fmaxs, label="fmax", color="red")
    ax2.set_ylabel("F max [eV / AA]")
    ax.grid()
    fig.legend()

    return fig


def mep_fig(path, energy):
    fig, ax = plt.subplots()
    ax.plot(path, energy, label="MEP")
    ax.grid()
    ax.set_title(f"Barrier height: {str(max(energy))[:5]} eV")
    ax.set_xlabel("Reaction Coordinate [AA]")
    ax.set_ylabel("Energy [eV]")
    ax.legend()

    return fig


class LogFile(io.TextIOWrapper):
    def write(self, d, encoding=sys.getdefaultencoding()):
        self.logger.info(d.strip())  # pylint: disable=no-member
        if isinstance(d, bytes):
            d = d.decode(encoding)
        super().write(d)


class DBWriter:
    def __init__(self, db_path, atomss):
        self.atomss = atomss
        self.db_path = db_path

    def write(self):
        with ase.db.connect(self.db_path) as db:
            for atoms in self.atomss:
                if atoms.calc.results:
                    db.write(atoms, data=atoms.calc.results)


def calculate_rc(atoms):
    dists = np.array(
        [0] + [distance(atom_1, atom_2) for atom_1, atom_2 in zip(atoms, atoms[1:])]
    )
    return np.cumsum(dists)


def distance(atom_1, atom_2):
    return kabsch_rmsd(atom_1.positions, atom_2.positions, translate=True)


def write_gif(mep, fp):
    write(fp, images=mep, format="gif")
