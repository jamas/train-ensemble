import json
from itertools import chain

import numpy as np
import torch
from pype import BaseConfig

from train_ensemble import data, utils


class Config(BaseConfig):
    script_path = "jobs/predict.py"
    inputs = {"data", "model"}
    params = {"batch_size": 100}
    outputs = {"force_norms.json": "force_norms.json", "energies.json": "energies.json"}


def main(config, logger):
    batch_size = config["params"]["batch_size"]
    data_configs = config["inputs"]["data"]

    datasets = data.get_datasets(data_configs)
    loaders = [
        data.get_dataloader(dataset, batch_size, shuffle=False) for dataset in datasets
    ]

    n_batches = sum(map(len, loaders))

    model = torch.load(config["inputs"]["model"])
    model.to(utils.DEVICE)

    force_norms = np.array([])
    energies = np.array([])

    for i, batch in enumerate(chain(*loaders)):
        if i % 100 == 0:
            logger.info(f"Calculated {i} of {n_batches} batches")

        batch = utils.batch_to_device(batch, utils.DEVICE)
        results = model(batch)

        forces = results["forces"].cpu().detach().numpy()
        shape = forces.shape
        flat_forces = forces.reshape(shape[0], np.product(shape[1:]))
        force_norms_ = np.linalg.norm(flat_forces, axis=1)
        force_norms = np.append(force_norms, force_norms_)

        energies = np.append(energies, results["energy"].cpu().detach().numpy())

    json.dump(energies.tolist(), open(config["outputs"]["energies.json"], "w"))
    json.dump(force_norms.tolist(), open(config["outputs"]["force_norms.json"], "w"))
