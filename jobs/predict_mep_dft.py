import json
import os
import shutil
import uuid

import ase.db
import ase.io
import numpy as np
from ase.neb import NEBTools
from pype import BaseConfig

from train_ensemble import calculator


class Config(BaseConfig):
    script_path = "jobs/predict_mep_dft.py"
    inputs = {"mep.xyz"}
    outputs = {"summary.json": "summary.json", "mep.db": "mep.db", "mep.xyz": "mep.xyz"}
    params = {"scratch": "/scratch/jamas"}


class DBWriter:
    def __init__(self, db_path, atomss):
        self.atomss = atomss
        self.db_path = db_path

    def write(self):
        with ase.db.connect(self.db_path) as db:
            for atoms in self.atomss:
                if atoms.calc.results:
                    db.write(atoms, data=atoms.calc.results)


def main(config, logger):
    scratch = config["params"]["scratch"]
    base = scratch if scratch else config["output_dir"]

    orcadir = os.path.join(base, uuid.uuid4().hex)
    logger.info(f"orca is using {orcadir} as working dir ...")

    calc_factory = calculator.OrcaCalcFactory(orcadir=orcadir)
    images = list(ase.io.iread(config["inputs"]["mep.xyz"]))
    logger.info(f"chemical formula: {str(images[0].symbols)}")
    for i, atoms in enumerate(images):
        logger.info(f"Calculating image {i} ")
        atoms.calc = calc_factory(cores=1, label=f"image{i}")
        atoms.get_forces()
    db_writer = DBWriter(config["outputs"]["mep.db"], images)
    db_writer.write()

    neb_tools = NEBTools(images)
    fit = neb_tools.get_fit()

    ase.io.write(config["outputs"]["mep.xyz"], images)

    summary = {
        "barrier": neb_tools.get_barrier()[0],
        "symbols": str(images[0].symbols),
        "energy": fit.fit_energies.tolist(),
        "path": fit.fit_path.tolist(),
        "ts_config": get_ts_config(images),
        "ts_energy": max(fit.fit_energies.tolist())
    }

    json.dump(summary, open(config["outputs"]["summary.json"], "w"))

    shutil.rmtree(orcadir)


def get_ts_config(images):
    energies = [image.calc.get_potential_energy() for image in images]
    ts_idx = np.argmax(energies)
    ts_config = images[ts_idx].positions.tolist()
    return ts_config
