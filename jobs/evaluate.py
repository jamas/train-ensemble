import json

import torch
from pype import BaseConfig

from train_ensemble import data, utils
from train_ensemble.evaluate import eval_model


class Config(BaseConfig):
    script_path = "jobs/evaluate.py"
    inputs = {"data", "model"}
    params = {"batch_size": 100}
    outputs = {"errors.json": "errors.json"}


def main(config, logger):
    batch_size = config["params"]["batch_size"]
    data_config = config["inputs"]["data"]

    logger.info("loading data")
    dataset = data.get_dataset(**data_config)
    loader = data.get_dataloader(dataset, batch_size, shuffle=True)

    logger.info("starting evaluation of data")
    model = torch.load(config["inputs"]["model"])
    model.to(utils.DEVICE)

    _, errors = eval_model(model, loader)
    del errors["val_loss"]

    json.dump(errors, open(config["outputs"]["errors.json"], 'w'))
