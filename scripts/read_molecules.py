import ase.db
from tqdm import tqdm
import json


d = {}

# path = 'storage/qm9x/qm9x.db'
path = '/Users/jamas/phd/research/transition1x/T1x/test.db'

with ase.db.connect(path) as cnx:
    for row in tqdm(cnx.select('')):
        if row.formula in d:
            d[row.formula] += [row.id - 1]

        else:
            d[row.formula] = [row.id - 1]

    json.dump(d, open(path.replace('.db', '.index.json'), 'w'))
