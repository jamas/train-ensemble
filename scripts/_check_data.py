"""
Run python check_data.py --db-path /path/to/db
to see available features and number of datapoints in dataset
"""

import logging

import schnetpack as spk

import ase.db

def main(args):  # pylint: disable=redefined-outer-name
    check_data(args.db_path)


def check_data(dbpath):
    dataset = ase.db.connect(dbpath)

    # dataset = spk.AtomsData(dbpath)
    logging.info(f"length of dataset {len(dataset)}")
    # nl = "\n"
    # logging.info(
    #     f"available properties in dataset are:{nl}"
    #     f"{nl.join(dataset.available_properties)}"
    # )


if __name__ == "__main__":
    from argparse import ArgumentParser

    logging.basicConfig(level=logging.INFO)
    parser = ArgumentParser()
    parser.add_argument("--db-path")
    args = parser.parse_args()
    main(args)
