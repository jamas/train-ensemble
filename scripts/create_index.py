import ase.db
import json
import tqdm


from argparse import ArgumentParser


def main(args): # pylint: disable=redefined-outer-name
    index = {}
    with ase.db.connect(args.db) as cnx:
        for row in tqdm.tqdm(cnx.select('id=1')):
            __import__("pdb").set_trace() #TODO delme
            if row.formula in index:
                index[row.formula] += [row.id-1]
            else:
                index[row.formula] = [row.id-1]


    json.dump(index, open(args.db.replace('.db', '.index.json'), 'w'))




if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('db')
    args = parser.parse_args()

    main(args)
