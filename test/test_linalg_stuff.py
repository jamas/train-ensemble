
import numpy as np
from pytest import approx

from jobs import path_distance


x00 = np.array([0, 0])
p00 = np.array([0,0])
p11 = np.array([1,1])
p01 = np.array([0,1])
p22 = np.array([2,2])
p12 = np.array([1,2])
p10 = np.array([1,0])
p0m1 = np.array([0,-1])
p1m1 = np.array([1,-1])
p11 = np.array([1,1])
pm1m1 = np.array([-1,-1])
pm2m2 = np.array([-2,-2])
p02 = np.array([0,2])
p20 = np.array([2,0])
p4m2 = np.array([4, -2])

def assert_approx(a, b):
    assert str(a)[:7]  == str(b)[:7]

def test_path_path_to_line():
    assert approx(path_distance.dist_line(x00, p11, p22 )) == 0
    assert approx(path_distance.dist_line(x00, p22, p11 )) == 0
    assert approx(path_distance.dist_line(x00, p0m1, p01 )) == 0
    assert approx(path_distance.dist_line(x00, p1m1, p11 )) == 1
    assert approx(path_distance.dist_line(x00, pm1m1, pm2m2 )) == 0
    assert approx(path_distance.dist_line(x00, p11, p12)) == 1
    assert approx(path_distance.dist_line(x00, p02, p20)) ==  np.sqrt(2)
    assert approx(path_distance.dist_line(x00, p01, p10)) ==  np.sqrt(2)/2
    assert approx(path_distance.dist_line(x00, p20, p02)) ==  np.sqrt(2)
    assert approx(path_distance.dist_line(x00, p20, p4m2)) == np.sqrt(2)
    assert approx(path_distance.dist_line(x00, p4m2, p02)) == np.sqrt(2)


def test_path_to_lineseg():
    assert path_distance.dist_lineseg(x00, p00, p22 ) == 0
    assert path_distance.dist_lineseg(x00, p0m1, p01 ) == 0
    assert path_distance.dist_lineseg(x00, p22, p11 ) == np.sqrt(2)
    assert path_distance.dist_lineseg(x00, p11, p22) == np.sqrt(2)
    assert path_distance.dist_lineseg(x00, p11, p22) == np.sqrt(2)
    assert path_distance.dist_lineseg(x00, pm1m1, pm2m2) == np.sqrt(2)
    assert path_distance.dist_lineseg(x00, p1m1, p11 ) == 1
    assert path_distance.dist_lineseg(x00, pm1m1, pm2m2 ) == np.sqrt(2)
    assert path_distance.dist_lineseg(x00, p11, p12 ) == np.sqrt(2)

# print(dist_point_to_curve(x, curve))
# plt.plot(*curve.T)
# plt.scatter(*x)
# plt.show()
