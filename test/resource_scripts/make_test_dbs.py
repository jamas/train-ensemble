import os

import ase.db
from ase import Atoms
from ase.calculators import EMT

rsc_dir = os.path.join("test", "resources")

H = Atoms("H")
He = Atoms("He")



with ase.db.connect(os.path.join(rsc_dir, "H.db")) as db:
    for i in range(100):
        H.set_positions([[i,0,0]])
        H.calc=EMT()
        H.get_potential_energy()
        db.write(H)


with ase.db.connect(os.path.join(rsc_dir, "He.db")) as db:
    for i in range(50):
        He.set_positions([[i,0,0]])
        He.calc=EMT()
        He.get_potential_energy()
        db.write(He)
