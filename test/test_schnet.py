import os

import torch

from train_ensemble.model import get_schnet
from train_ensemble import utils


def test_load(tmpdir, batch):
    fresh_schnet = get_schnet("energy", "forces")
    fresh_schnet.to(utils.DEVICE)
    batch = utils.batch_to_device(batch, utils.DEVICE)
    path = os.path.join(tmpdir, "model.pt")
    torch.save(fresh_schnet, path)
    loaded_schnet = torch.load(path, map_location="cpu")
    loaded_schnet = torch.load("/Users/jamas/phd/research/train-ensemble/pipelines/reproduce_gpu_error/train_schnet/model_dir/best")

    # fresh_schnet(batch)
    loaded_schnet(batch)


def test_fresh_schnet_can_forward(batch):
    schnet = get_schnet("energy", "forces")
    schnet(batch)


# def test_loaded_schnet_can_forward(batch):
#     schnet = torch.load("test/resources/cpu_schnet.pt")
#     schnet(batch)
