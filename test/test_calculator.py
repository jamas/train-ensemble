from train_ensemble import calculator
from train_ensemble.model.schnet import get_schnet


def test_atom(atoms):
    schnet = get_schnet(energy_key = "energy", force_key="forces")
    atoms.calc = calculator.MLCalculator(schnet)
    atoms.get_potential_energy()
    atoms.get_forces()
