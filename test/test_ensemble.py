import torch
import os

from train_ensemble.model.ensemble import Ensemble


def test_ensemble(batch):
    paths = ["test/resources/gpu_schnet.pth", "test/resources/cpu_schnet.pth"]
    ensemble = Ensemble(paths, energy_key="energy", force_key="force")

    pred = ensemble(batch)
    assert pred["energy"].shape == pred["energy_var"].shape
    assert pred["force"].shape == pred["force_var"].shape


def test_torch_can_load_gpu_and_cpu():
    torch.load("test/resources/gpu_schnet.pt", map_location="cpu")
    torch.load("test/resources/cpu_schnet.pt", map_location="cpu")


def test_can_load_ensemble(batch, tmpdir):
    paths = ["test/resources/gpu_schnet.pth", "test/resources/cpu_schnet.pth"]
    ensemble = Ensemble(paths, energy_key="energy", force_key="force")
    path = os.path.join(tmpdir, "model.pt")
    torch.save(ensemble, path)
    loaded_ensemble = torch.load(path, map_location="cpu")
    pred = loaded_ensemble(batch)

    assert pred["energy"].shape == pred["energy_var"].shape
    assert pred["force"].shape == pred["force_var"].shape
