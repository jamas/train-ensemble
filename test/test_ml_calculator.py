import torch
import pytest
from train_ensemble.calculator import MLCalculator


class MockModel(torch.nn.Module):
    def forward(self, _):  # pylint: disable=no-self-use
        energy_var = torch.Tensor([[26191.6797]])
        energy = torch.Tensor([[4.6800]])
        forces = torch.Tensor(
            [
                [
                    [0.1448, 0.0056, -0.0882],
                    [-0.0453, -0.0426, 0.0959],
                    [-0.3598, -0.1459, 0.1635],
                    [0.2048, 0.2043, 0.0372],
                    [-0.0602, 0.0685, -0.0446],
                    [0.2211, -0.0743, -0.1556],
                    [0.2123, 0.0990, -0.1747],
                    [-0.2742, -0.0831, 0.1169],
                    [0.3680, 0.0902, -0.0582],
                    [0.0584, -0.0199, 0.0346],
                    [-0.4697, -0.1019, 0.0731],
                ]
            ]
        )
        forces_var = torch.Tensor(
            [
                [
                    [4.6692e-01, 3.2701e-03, 7.0538e-01],
                    [4.8091e-02, 7.9389e-02, 1.5244e-01],
                    [2.2888e-02, 1.3377e-01, 2.7321e-01],
                    [6.0042e-03, 2.1424e-05, 3.4876e-02],
                    [5.2183e-01, 1.6319e-01, 1.0240e-01],
                    [5.4938e-04, 4.8139e-02, 1.4276e-02],
                    [1.6579e-01, 1.7874e-02, 1.0110e-01],
                    [5.8693e-02, 1.5494e-02, 1.8975e-03],
                    [4.3780e-01, 2.1658e-01, 2.9573e-01],
                    [6.4241e-02, 9.4255e-02, 4.5742e-01],
                    [1.3549e-03, 4.9410e-03, 3.7261e-03],
                ]
            ]
        )

        return {
            "energy": energy,
            "energy_var": energy_var,
            "forces": forces,
            "forces_var": forces_var,
        }


@pytest.fixture(name="ml_calculator")
def ml_calculator_fixture():
    return MLCalculator(
        MockModel(),
        lambda x: x,
        implemented_properties=['energy', 'forces', 'energy_var', 'forces_var']
    )


def test_var(atoms, ml_calculator):
    atoms.calc = ml_calculator
    atoms.get_potential_energy()
    atoms.calc.get_property("energy")
    atoms.calc.get_property("energy_var")
    atoms.calc.get_property("forces")
    atoms.calc.get_property("forces_var")
