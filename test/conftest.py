import pytest

from schnetpack.data.atoms import AtomsConverter
from ase.io import read


atoms_converter = AtomsConverter()
TEST_XYZ = "test/resources/atoms.xyz"


@pytest.fixture
def atoms():
    return read(TEST_XYZ)

@pytest.fixture
def batch(atoms):
    batch = atoms_converter(atoms)

    return batch
