from train_ensemble import data
import pytest
from train_ensemble import rotating_loader

get_dataloaders = data.get_gnn_dataloaders


@pytest.fixture()
def loaders():
    train_loader, val_loader, _ = get_dataloaders(
        "test/resources/H.db",
        "energy",
        "forces",
        "pipelines/split_ani1x/split/output/split.npz",
        10,
    )
    return train_loader, val_loader



from itertools import cycle

def test_two_dataloaders(loaders):
    loader1, loader2 = loaders
    __import__("pdb").set_trace() #TODO delme

