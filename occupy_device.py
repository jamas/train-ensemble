from argparse import ArgumentParser
import os
import time

import torch


def main(args):
    os.environ['CUDA_VISIBLE_DEVICES'] = args.device
    t = torch.Tensor([0])
    t.to('cuda')
    time.sleep(int(args.time))


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('--device', required=True)
    parser.add_argument('--time', default=900)
    args = parser.parse_args()

    main(args)
