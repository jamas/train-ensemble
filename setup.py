from setuptools import setup, find_packages

setup(
    name="train_ensemble",
    version="1.0",
    packages=find_packages(),
    install_requires=[
        "ase",
        "schnetpack",
        "pandas",
        "autochem",
        "pype-ms",
        "rmsd",
        "scipy",
        "GPUtil",
        # 'graphnn @ git+ssh://git@gitlab.gbar.dtu.dk:sure/graphnn.git@graphnn-0.0.1'
        # "graphnn @ git@gitlab.gbar.dtu.dk:sure/graphnn.git",
        # 'graphnn @ git+ssh://git@github.com/someorgname/pkg-repo-name@v1.1#egg=some-pkg',
        # "git+ssh://git@gitlab.gbar.dtu.dk:sure/graphnn.git@graphnn-0.0.1",
        #'graphnn @ git+ssh://git@github.com/someorgname/pkg-repo-name@v1.1#egg=some-pkg',
        # https://gitlab.gbar.dtu.dk/sure/graphnn
    ],
)
